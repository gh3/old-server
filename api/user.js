var config = require('../core/config.js');
var secure = require('../core/secure.js');
var mongo = require('../core/mongo.js');
var sms = require('../utils/sms.js');
var ObjectId = require('mongodb').ObjectID;
var api = {};

api['user/has-name.json'] = function (req, res) {
    var user = req.user;

    mongo.db.collection('users').findOne({
        _id: ObjectId(user._id)
    }, function(err, user) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!user) {
            res.send({
                status: 'error',
                msg: 'User does not exist.'
            });
        } else {
            res.send({
                status: 'ok',
                data: {
                    hasName: user.hasOwnProperty('name')
                }
            });
        }
    });
};

api['user/update-name.json'] = function (req, res) {
    var user = req.user;
    var name = req.payload.name;

    if (!name) {
        res.send({
            status: 'error',
            msg: 'Name is missing!'
        });
        return;
    }

    mongo.db.collection('users').update({
        _id: ObjectId(user._id)
    }, {
        $set: {
            name: name
        }
    }, function(err, mgr) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!mgr.result.ok || !mgr.result.n) {
            res.send({
                status: 'error',
                msg: 'Database updating error!'
            });
        } else {
            res.send({
                status: 'ok',
                msg: 'Your name was successfully updated!'
            });
        }
    });
};

api['user/request-phone-update.json'] = function (req, res) {
    var user = req.user;
    var phone = req.payload.phone;
    var areacode = req.payload.areacode;

    if (!areacode) {
        res.send({
            status: 'error',
            msg: 'You have to select your area!'
        });
        return;
    }

    // convert double zero into plus
    if (areacode.substring(0,2) === '00') {
        areacode = '+' + areacode.slice(2);
    }

    // starting with plus sign
    if (areacode.charAt(0) !== '+') {
        res.send({
            status: 'error',
            msg: 'Area code should start with plus sign!'
        });
        return;
    }

    if (areacode.length < 2) {
        res.send({
            status: 'error',
            msg: 'Area code is too short!'
        });
        return;
    }

    if (!phone) {
        res.send({
            status: 'error',
            msg: 'Phone is missing!'
        });
        return;
    }

    // convert international phone number to local
    phone = phone.replace(areacode, 0);

    // starting with digit zero
    if (phone.charAt(0) !== '0') {
        res.send({
            status: 'error',
            msg: 'Phone should start with digit zero!'
        });
        return;
    }

    // only digits
    var isNum = true;
    isNum = /^[\d+]+$/.test(phone);
    if (!isNum) {
        res.send({
            status: 'error',
            msg: 'Phone should contain only digits.'
        });
        return;
    }

    if (phone.length < config.minPhoneLength) {
        res.send({
            status: 'error',
            msg: 'Phone is too short!'
        });
        return;
    }

    // users collections
    var users = mongo.db.collection('users');

    // finding the user
    users.findOne({
        $and: [{
            _id: {
                $ne: ObjectId(user._id)
            }
        }, {
            $or: [{
                phone: phone
            }, {
                "phoneUpdate.phone": phone
            }]
        }]
    }, function(err, userWithPhone) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (userWithPhone) {
            res.send({
                status: 'error',
                msg: 'Phone number ' + phone + ' is already associated with another user!'
            });
        } else {
            mongo.db.collection('areacodes').findOne({
                areacode: areacode
            }, function(err, area) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!area) {
                    res.send({
                        status: 'error',
                        msg: 'We sadly do not support area ' + areacode + ' yet!'
                    });
                } else {
                    // phone for SMS
                    var phoneSMS = area.areacode + phone.substr(1);
                    var areacodeId = area._id;

                    // finding the user
                    users.findOne({
                        _id: ObjectId(user._id)
                    }, function(err, user) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else if (!user) {
                            res.send({
                                status: 'out',
                                msg: 'You do not exist any more in our system.'
                            });
                        } else {
                            if (user.hasOwnProperty('phoneUpdate')) {
                                res.send({
                                    status: 'error',
                                    msg: 'Phone update request was made not a log time ago. Please confirm it or remove it.'
                                });
                                return;
                            }

                            if (!user.hasOwnProperty('phoneSMS')) {
                                res.send({
                                    status: 'error',
                                    msg: 'We sadly can not send you URL to confirm phone update right now!',
                                    error: 'Phone number for sending text messages does not exist for this user.'
                                });
                                return;
                            }

                            if (user.phoneSMS === phoneSMS) {
                                res.send({
                                    status: 'error',
                                    msg: 'Your new phone is the same as old one.'
                                });
                                return;
                            }

                            // generating random token from readable letters
                            var token = '';
                            for (var i = 0; i < config.tokenLength; i++) {
                                token += config.readableLetters.charAt(Math.floor(Math.random() * config.readableLetters.length));
                            }

                            // updating user
                            users.update({
                                _id: ObjectId(user._id)
                            }, {
                                $set: {
                                    phoneUpdate: {
                                        phone: phone,
                                        phoneSMS: phoneSMS,
                                        areacodeId: areacodeId,
                                        token: token,
                                        created: new Date().getTime()
                                    }
                                }
                            }, function(err, mgr) {
                                if (err) {
                                    res.send({
                                        status: 'error',
                                        msg: 'Database error!',
                                        error: err.message
                                    });
                                } else if (!mgr.result.ok || !mgr.result.n) {
                                    res.send({
                                        status: 'error',
                                        msg: 'Database updating error!'
                                    });
                                } else {
                                    // sending sms with url to recover password
                                    var textMessage = 'Please confirm your new phone number here http://gh3.co/#/' + config.tokenAbbreviation.phoneUpdate + '/' + token + ' or ignore it if you do not know what is this about.';
                                    sms.send(user._id, phoneSMS, textMessage, function(err, utr) {
                                        if (err) {
                                            res.send({
                                                status: 'ok',
                                                msg: 'Phone update was saved into database, but something went wrong with sending text message to ' + phoneSMS + ' phone number!',
                                                error: err.error,
                                                data: {
                                                    token: token
                                                }
                                            });
                                        } else {
                                            res.send({
                                                status: 'ok',
                                                msg: 'Phone update almost done. We sent you a text message to ' + phoneSMS + ' with confirmation URL.',
                                                data: {
                                                    token: token
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

api['user/confirm-phone-update.json'] = function (req, res) {
    var token = req.payload.token;

    if (!token) {
        res.send({
            status: 'error',
            msg: 'Token is missing!'
        });
        return;
    }

    if (token.length !== config.tokenLength) {
        res.send({
            status: 'error',
            msg: 'Token is corrupted!'
        });
        return;
    }

    var users = mongo.db.collection('users');

    users.findOne({
        'phoneUpdate.token': token
    }, function(err, user) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!user) {
            res.send({
                status: 'error',
                msg: 'We can not update your phone. Token does not belong to any user.'
            });
        } else {
            var phoneUpdate = user.phoneUpdate;

            users.findOne({
                phone: phoneUpdate.phone
            }, function(err, userWithPhone) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (userWithPhone) {
                    res.send({
                        status: 'error',
                        msg: 'We can not update your phone. It looks like user with phone ' + phoneUpdate.phone + ' already exists.'
                    });
                } else {
                    // updating user
                    users.update({
                        _id: ObjectId(user._id)
                    }, {
                        $set: {
                            phone: phoneUpdate.phone,
                            phoneSMS: phoneUpdate.phoneSMS,
                            areacodeId: phoneUpdate.areacodeId
                        },
                        $unset: {
                            'phoneUpdate': true
                        }
                    }, function(err, mgr) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else if (!mgr.result.ok || !mgr.result.n) {
                            res.send({
                                status: 'error',
                                msg: 'Database updating error!'
                            });
                        } else {
                            res.send({
                                status: 'ok',
                                msg: 'Your phone number is now updated!'
                            });
                        }
                    });
                }
            });
        }
    });
};

api['user/remove-phone-update.json'] = function (req, res) {
    var user = req.user;

    var users = mongo.db.collection('users');

    users.findOne({
        _id: ObjectId(user._id)
    }, function(err, user) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!user) {
            res.send({
                status: 'out',
                msg: 'You do not exist any more in our system.'
            });
        } else {
            if (!user.hasOwnProperty('phoneUpdate')) {
                res.send({
                    status: 'error',
                    msg: 'Phone update request does not exist!'
                });
                return;
            }

            users.update({
                _id: ObjectId(user._id)
            }, {
                $unset: {
                    'phoneUpdate': true
                }
            }, function(err, mgr) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!mgr.result.ok || !mgr.result.n) {
                    res.send({
                        status: 'error',
                        msg: 'Database updating error!'
                    });
                } else {
                    res.send({
                        status: 'ok',
                        msg: 'Phone update request removed!'
                    });
                }
            });
        }
    });
};

api['user/is-invited-noobie.json'] = function (req, res) {
    var user = req.user;

    mongo.db.collection('users').findOne({
        _id: ObjectId(user._id)
    }, function(err, user) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!user) {
            res.send({
                status: 'error',
                msg: 'User does not exist.'
            });
        } else {
            res.send({
                status: 'ok',
                data: {
                    invitedNoobie: user.hasOwnProperty('invitedNoobie')
                }
            });
        }
    });
};

api['user/password-set.json'] = function (req, res) {
    var user = req.user;
    var newPassword = req.payload.newPassword;
    var passwordRepeat = req.payload.passwordRepeat;
    var oldPassword = req.payload.oldPassword;

    if (!newPassword) {
        res.send({
            status: 'error',
            msg: 'New password is missing!'
        });
        return;
    }

    if (newPassword.length < config.minPasswordLength) {
        res.send({
            status: 'error',
            msg: 'New password is too short!'
        });
        return;
    }

    if (!passwordRepeat) {
        res.send({
            status: 'error',
            msg: 'Password repeat is missing!'
        });
        return;
    }

    if (passwordRepeat.length < config.minPasswordLength) {
        res.send({
            status: 'error',
            msg: 'Password repeat is too short!'
        });
        return;
    }

    if (newPassword !== passwordRepeat) {
        res.send({
            status: 'error',
            msg: 'New password and password repeat are not the same. They should be!'
        });
        return;
    }

    if (!user.hasOwnProperty('invitedNoobie')) {

        if (!oldPassword) {
            res.send({
                status: 'error',
                msg: 'Old password is missing!'
            });
            return;
        }

        if (oldPassword.length < config.minPasswordLength) {
            res.send({
                status: 'error',
                msg: 'Old password is too short!'
            });
            return;
        }

        if (newPassword === oldPassword) {
            res.send({
                status: 'error',
                msg: 'New password can not be the same as old password!'
            });
            return;
        }
    }

    var users = mongo.db.collection('users');

    users.findOne({
        _id: ObjectId(user._id)
    }, function(err, user) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!user) {
            res.send({
                status: 'out',
                msg: 'You do not exist any more in our system.'
            });
        } else {
            var newHash = require('crypto').createHash('sha256').update(newPassword + secure.salt).digest('hex');

            if (!user.hasOwnProperty('invitedNoobie')) {
                var oldHash = require('crypto').createHash('sha256').update(oldPassword + secure.salt).digest('hex');

                if (oldHash !== user.password) {
                    res.send({
                        status: 'error',
                        msg: 'Old password is wrong!'
                    });
                    return;
                }
            }

            // updating user
            users.update({
                _id: ObjectId(user._id)
            }, {
                $set: {
                    password: newHash
                },
                $unset: {
                    'invitedNoobie': true
                }
            }, function(err, mgr) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!mgr.result.ok || !mgr.result.n) {
                    res.send({
                        status: 'error',
                        msg: 'Database updating error!'
                    });
                } else {
                    res.send({
                        status: 'ok',
                        msg: 'New password is set!'
                    });
                }
            });
        }
    });
};

module.exports = api;
