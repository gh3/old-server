var config = require('../core/config.js');
var mongo = require('../core/mongo.js');
var ObjectId = require('mongodb').ObjectID;
var api = {};

api['users/find.json'] = function (req, res) {
    var phone = req.payload.phone || '';

    // users collections
    var users = mongo.db.collection('users');

    // finding users
    users.find({phone: {
        $regex: phone,
        $options: 'i'
    }}).toArray(function(err, docs) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else {
            res.send({
                status: 'ok',
                msg: 'Here is the list of users' + (phone ? ' filtered by \'' + phone + '\' phone' : '') + '!',
                data: {
                    users: docs
                }
            });
        }
    });
};

module.exports = api;
