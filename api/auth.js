var config = require('../core/config.js');
var secure = require('../core/secure.js');
var sms = require('../utils/sms.js');
var session = require('../utils/session.js');
var mongo = require('../core/mongo.js');
var ObjectId = require('mongodb').ObjectID;
var api = {};

api['auth/register.json'] = function (req, res) {
    var phone = req.payload.phone;
    var password = req.payload.password;
    var areacode = req.payload.areacode;
    var doublecheck = req.payload.doublecheck;

    if (!areacode) {
        res.send({
            status: 'error',
            msg: 'You have to select your area!'
        });
        return;
    }

    // convert double zero into plus
    if (areacode.substring(0,2) === '00') {
        areacode = '+' + areacode.slice(2);
    }

    // starting with plus sign
    if (areacode.charAt(0) !== '+') {
        res.send({
            status: 'error',
            msg: 'Area code should start with plus sign!'
        });
        return;
    }

    if (areacode.length < 2) {
        res.send({
            status: 'error',
            msg: 'Area code is too short!'
        });
        return;
    }

    if (!phone) {
        res.send({
            status: 'error',
            msg: 'Phone is missing!'
        });
        return;
    }

    // convert international phone number to local
    phone = phone.replace(areacode, 0);

    // starting with digit zero
    if (phone.charAt(0) !== '0') {
        res.send({
            status: 'error',
            msg: 'Phone should start with digit zero!'
        });
        return;
    }

    // only digits
    var isNum = true;
    isNum = /^[\d+]+$/.test(phone);
    if (!isNum) {
        res.send({
            status: 'error',
            msg: 'Phone should contain only digits.'
        });
        return;
    }

    if (phone.length < config.minPhoneLength) {
        res.send({
            status: 'error',
            msg: 'Phone is too short!'
        });
        return;
    }

    if (!password) {
        res.send({
            status: 'error',
            msg: 'Password is missing!'
        });
        return;
    }

    // password length
    if (password.length < config.minPasswordLength) {
        res.send({
            status: 'error',
            msg: 'Password is too short!'
        });
        return;
    }

    mongo.db.collection('areacodes').findOne({
        areacode: areacode
    }, function(err, area) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!area) {
            res.send({
                status: 'error',
                msg: 'We sadly do not support area ' + areacode + ' yet!'
            });
        } else {
            // phone for SMS
            var phoneSMS = area.areacode + phone.substr(1);

            // users collections
            var users = mongo.db.collection('users');

            // finding the user
            users.findOne({
                phone: phone
            }, function(err, user) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (user) {
                    res.send({
                        status: 'error',
                        msg: 'User with phone ' + phone + ' already exists!'
                    });
                } else if (!doublecheck) {
                    res.send({
                        status: 'error',
                        msg: 'Please double check the phone number!',
                        error: 'doublecheck',
                        data: {
                            phoneSMS: phoneSMS
                        }
                    });
                } else {
                    var hash = require('crypto').createHash('sha256').update(password + secure.salt).digest('hex');

                    users.insert({
                        phone: phone,
                        phoneSMS: phoneSMS,
                        password: hash,
                        created: new Date().getTime(),
                        areacodeId: ObjectId(area._id)
                    }, function(err, mgr) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else if (!mgr.result.ok || !mgr.result.n) {
                            res.send({
                                status: 'error',
                                msg: 'Database inserting error!'
                            });
                        } else {
                            user = mgr.ops[0];

                            session.set(user._id, user.phone, user.password, req.headers.host, req.headers['user-agent'], function(err, utr) {
                                if (err) {
                                    res.send({
                                        status: 'error',
                                        msg: 'Thank you for registration. We were not able to establish session for you automatically, but you can try to login on your own!',
                                        error: err.error
                                    });
                                } else {
                                    sessionHash = utr.session;

                                    sms.send(user._id, phoneSMS, 'Welcome to http://gh3.co/ community!', function(err, utr) {
                                        if (err) {
                                            res.send({
                                                status: 'ok',
                                                msg: 'Thank you for registration. We can not send you welcome text message currently, but you can totally login already!',
                                                data: {
                                                    session: sessionHash,
                                                    userId: user._id
                                                }
                                            });
                                        } else {
                                            res.send({
                                                status: 'ok',
                                                msg: 'Thank you for registration. You will receive a welcome text message on your phone. Go ahead and login!',
                                                data: {
                                                    session: sessionHash,
                                                    userId: user._id
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

api['auth/login.json'] = function (req, res) {
    var phone = req.payload.phone;
    var password = req.payload.password;
    var areacode = req.payload.areacode;

    if (!areacode) {
        res.send({
            status: 'error',
            msg: 'You have to select your area!'
        });
        return;
    }

    // convert double zero into plus
    if (areacode.substring(0,2) === '00') {
        areacode = '+' + areacode.slice(2);
    }

    // starting with plus sign
    if (areacode.charAt(0) !== '+') {
        res.send({
            status: 'error',
            msg: 'Area code should start with plus sign!'
        });
        return;
    }

    if (areacode.length < 2) {
        res.send({
            status: 'error',
            msg: 'Area code is too short!'
        });
        return;
    }

    if (!phone) {
        res.send({
            status: 'error',
            msg: 'Phone is missing!'
        });
        return;
    }

    // convert international phone number to local
    phone = phone.replace(areacode, 0);

    // starting with digit zero
    if (phone.charAt(0) !== '0') {
        res.send({
            status: 'error',
            msg: 'Phone should start with digit zero!'
        });
        return;
    }

    // only digits
    var isNum = true;
    isNum = /^[\d+]+$/.test(phone);
    if (!isNum) {
        res.send({
            status: 'error',
            msg: 'Phone should contain only digits.'
        });
        return;
    }

    if (phone.length < config.minPhoneLength) {
        res.send({
            status: 'error',
            msg: 'Phone is too short!'
        });
        return;
    }

    if (!password) {
        res.send({
            status: 'error',
            msg: 'Password is missing!'
        });
        return;
    }

    // password length
    if (password.length < config.minPasswordLength) {
        res.send({
            status: 'error',
            msg: 'Password is too short!'
        });
        return;
    }

    mongo.db.collection('areacodes').findOne({
        areacode: areacode
    }, function(err, area) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!area) {
            res.send({
                status: 'error',
                msg: 'We sadly do not support area ' + areacode + ' yet!'
            });
        } else {
            var areacodeId = area._id;
            var hash = require('crypto').createHash('sha256').update(password + secure.salt).digest('hex');

            // users collections
            var users = mongo.db.collection('users');

            // finding the user
            users.findOne({
                phone: phone,
                areacodeId: ObjectId(areacodeId)
            }, function(err, user) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (user) {
                    if (hash === user.password) {
                        // login directly if hash looks okay
                        session.set(user._id, user.phone, user.password, req.headers.host, req.headers['user-agent'], function(err, utr) {
                            if (err) {
                                res.send({
                                    status: 'error',
                                    msg: 'We were not able to establish session for you.',
                                    error: err.error
                                });
                            } else {
                                res.send({
                                    status: 'ok',
                                    msg: 'Login succeeded!',
                                    data: {
                                        session: utr.session,
                                        userId: user._id
                                    }
                                });
                            }
                        });
                    } else if (user.hasOwnProperty('passwordReset') && password.toUpperCase() === user.passwordReset.plain.toUpperCase()) {
                        hash = require('crypto').createHash('sha256').update(user.passwordReset.plain + secure.salt).digest('hex');
                        // check for password reset field
                        session.set(user._id, user.phone, user.password, req.headers.host, req.headers['user-agent'], function(err, utr) {
                            if (err) {
                                res.send({
                                    status: 'error',
                                    msg: 'We were not able to establish session for you.',
                                    error: err.error
                                });
                            } else {
                                // clean up password reset
                               mongo.db.collection('users').update({
                                    _id: ObjectId(user._id)
                                }, {
                                    $set: {
                                        password: hash
                                    },
                                    $unset: {
                                        'passwordReset': true
                                    }
                                }, function(err, mgr) {
                                    if (err) {
                                        res.send({
                                            status: 'error',
                                            msg: 'Database error!',
                                            error: err.message
                                        });
                                    } else if (!mgr.result.ok || !mgr.result.n) {
                                        res.send({
                                            status: 'error',
                                            msg: 'Database updating error!'
                                        });
                                    } else {
                                        res.send({
                                            status: 'ok',
                                            msg: 'Login succeeded!',
                                            data: {
                                                session: utr.session,
                                                userId: user._id
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        res.send({
                            status: 'error',
                            msg: 'Wrong password!'
                        });
                    }
                } else {
                    res.send({
                        status: 'error',
                        msg: 'Login did not succeed!'
                    });
                }
            });
        }
    });
};

api['auth/password.json'] = function (req, res) {

    var password = req.payload.password;
    if (!password || password === '') {
        res.send({
            status: 'error',
            msg: 'Password is missing!'
        });
        return;
    }

    var hash = require('crypto').createHash('sha256').update(password + secure.salt).digest('hex');

    res.send({
        status: 'ok',
        msg: 'Well, this how hashed password looks like!',
        data: {
            password: req.payload.password,
            hash: hash
        }
    });
};

api['auth/crypte.json'] = function (req, res) {

    var text = req.payload.text;

    if (!text || text === '') {
        res.send({
            status: 'error',
            msg: 'Text is missing!'
        });
        return;
    }

    var crypto = require('crypto');

    var algorithm = 'aes256';
    var key = secure.key;

    var cipher = crypto.createCipher(algorithm, key);
    var encrypted = cipher.update(text, 'utf8', 'hex') + cipher.final('hex');

    var decipher = crypto.createDecipher(algorithm, key);
    var decrypted = decipher.update(encrypted, 'hex', 'utf8') + decipher.final('utf8');

    res.send({
        status: 'ok',
        msg: 'Well, this is how it looks to encrypte and decrypte a text!',
        date: {
            text: text,
            encrypted: encrypted,
            decrypted: decrypted
        }
    });
}

api['auth/whoami.json'] = function (req, res) {
    var user = req.user;

    res.send({
        status: 'ok',
        msg: 'Well, looks like you are logged in!!',
        data: {
            whoami: user
        }
    });
};

api['auth/password-reset.json'] = function (req, res) {
    var phone = req.payload.phone;
    var areacode = req.payload.areacode;

    if (!areacode) {
        res.send({
            status: 'error',
            msg: 'You have to select your area!'
        });
        return;
    }

    // convert double zero into plus
    if (areacode.substring(0,2) === '00') {
        areacode = '+' + areacode.slice(2);
    }

    // starting with plus sign
    if (areacode.charAt(0) !== '+') {
        res.send({
            status: 'error',
            msg: 'Area code should start with plus sign!'
        });
        return;
    }

    if (areacode.length < 2) {
        res.send({
            status: 'error',
            msg: 'Area code is too short!'
        });
        return;
    }

    if (!phone) {
        res.send({
            status: 'error',
            msg: 'Phone is missing!'
        });
        return;
    }

    // convert international phone number to local
    phone = phone.replace(areacode, 0);

    // starting with digit zero
    if (phone.charAt(0) !== '0') {
        res.send({
            status: 'error',
            msg: 'Phone should start with digit zero!'
        });
        return;
    }

    // only digits
    var isNum = true;
    isNum = /^[\d+]+$/.test(phone);
    if (!isNum) {
        res.send({
            status: 'error',
            msg: 'Phone should contain only digits.'
        });
        return;
    }

    if (phone.length < config.minPhoneLength) {
        res.send({
            status: 'error',
            msg: 'Phone is too short!'
        });
        return;
    }

    mongo.db.collection('areacodes').findOne({
        areacode: areacode
    }, function(err, area) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!area) {
            res.send({
                status: 'error',
                msg: 'We sadly do not support area ' + areacode + ' yet!'
            });
        } else {
            var areacodeId = area._id;

            // users collections
            var users = mongo.db.collection('users');

            // finding the user
            users.findOne({
                phone: phone,
                areacodeId: ObjectId(areacodeId)
            }, function(err, user) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!user) {
                    res.send({
                        status: 'error',
                        msg: 'User with ' + phone + ' phone number in area code ' + areacode + ' does not exist!'
                    });
                } else {
                    if (user.hasOwnProperty('passwordReset')) {
                        var timestamp = new Date().getTime();
                        var passwordCreated = user.passwordReset.created;
                        var elapsedMinutes = Math.floor((timestamp - passwordCreated) / (1000 * 60));
                        var minutesToGo = config.passwordResetWaitTime - elapsedMinutes;

                        if (minutesToGo > 0) {
                            res.send({
                                status: 'error',
                                msg: 'Password reset was requested not long time ago. Hold on for ' + minutesToGo + ' more min and you will be able to reset password again.'
                            });
                            return;
                        }
                    }

                    if (!user.hasOwnProperty('phoneSMS')) {
                        res.send({
                            status: 'error',
                            msg: 'We sadly can not reset your password at the moment!',
                            error: 'Phone number for sending text messages does not exist for this user.'
                        });
                        return;
                    } 

                    // generating random password from readable letters
                    var generatedPassword = '';
                    for (var i = 0; i < config.minPasswordLength; i++) {
                        generatedPassword += config.readableLetters.charAt(Math.floor(Math.random() * config.readableLetters.length));
                    }
                    generatedPassword = generatedPassword.charAt(0).toUpperCase() + generatedPassword.slice(1);

                    // updating user
                    users.update({
                        _id: ObjectId(user._id)
                    }, {
                        $set: {
                            passwordReset: {
                                plain: generatedPassword,
                                created: new Date().getTime()
                            }
                        }
                    }, function(err, mgr) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else if (!mgr.result.ok || !mgr.result.n) {
                            res.send({
                                status: 'error',
                                msg: 'Database updating error!'
                            });
                        } else {
                            // sending sms with new password
                            sms.send(user._id, user.phoneSMS, 'Your new password: ' + generatedPassword, function(err, utr) {
                                if (err) {
                                    res.send({
                                        status: 'error',
                                        msg: 'Sending new password via text message failed! Please try to reset your password once again.',
                                        error: err.error
                                    });
                                } else {
                                    res.send({
                                        status: 'ok',
                                        msg: 'You will receive new password on your phone via text message!'
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

api['auth/password-recovery.json'] = function (req, res) {
    var phone = req.payload.phone;
    var areacode = req.payload.areacode;

    if (!areacode) {
        res.send({
            status: 'error',
            msg: 'You have to select your area!'
        });
        return;
    }

    // convert double zero into plus
    if (areacode.substring(0,2) === '00') {
        areacode = '+' + areacode.slice(2);
    }

    // starting with plus sign
    if (areacode.charAt(0) !== '+') {
        res.send({
            status: 'error',
            msg: 'Area code should start with plus sign!'
        });
        return;
    }

    if (areacode.length < 2) {
        res.send({
            status: 'error',
            msg: 'Area code is too short!'
        });
        return;
    }

    if (!phone) {
        res.send({
            status: 'error',
            msg: 'Phone is missing!'
        });
        return;
    }

    // convert international phone number to local
    phone = phone.replace(areacode, 0);

    // starting with digit zero
    if (phone.charAt(0) !== '0') {
        res.send({
            status: 'error',
            msg: 'Phone should start with digit zero!'
        });
        return;
    }

    // only digits
    var isNum = true;
    isNum = /^[\d+]+$/.test(phone);
    if (!isNum) {
        res.send({
            status: 'error',
            msg: 'Phone should contain only digits.'
        });
        return;
    }

    if (phone.length < config.minPhoneLength) {
        res.send({
            status: 'error',
            msg: 'Phone is too short!'
        });
        return;
    }

    mongo.db.collection('areacodes').findOne({
        areacode: areacode
    }, function(err, area) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!area) {
            res.send({
                status: 'error',
                msg: 'We sadly do not support area ' + areacode + ' yet!'
            });
        } else {
            var areacodeId = area._id;

            // users collections
            var users = mongo.db.collection('users');

            // finding the user
            users.findOne({
                phone: phone,
                areacodeId: ObjectId(areacodeId)
            }, function(err, user) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!user) {
                    res.send({
                        status: 'error',
                        msg: 'User with ' + phone + ' phone number in area code ' + areacode + ' does not exist!'
                    });
                } else {
                    if (user.hasOwnProperty('passwordRecovery')) {
                        var timestamp = new Date().getTime();
                        var tokenCreated = user.passwordRecovery.created;
                        var elapsedMinutes = Math.floor((timestamp - tokenCreated) / (1000 * 60));
                        var minutesToGo = config.passwordRecoveryWaitTime - elapsedMinutes;

                        if (minutesToGo > 0) {
                            res.send({
                                status: 'error',
                                msg: 'Password recovery was requested not long time ago. Hold on for ' + minutesToGo + ' more min and you will be able to recover password again.'
                            });
                            return;
                        }
                    }

                    if (!user.hasOwnProperty('phoneSMS')) {
                        res.send({
                            status: 'error',
                            msg: 'We sadly can not send you recovery URL at the moment!',
                            error: 'Phone number for sending text messages does not exist for this user.'
                        });
                        return;
                    } 

                    // generating random token from readable letters
                    var token = '';
                    for (var i = 0; i < config.tokenLength; i++) {
                        token += config.readableLetters.charAt(Math.floor(Math.random() * config.readableLetters.length));
                    }

                    // updating user
                    users.update({
                        _id: ObjectId(user._id)
                    }, {
                        $set: {
                            passwordRecovery: {
                                token: token,
                                created: new Date().getTime()
                            }
                        }
                    }, function(err, mgr) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else if (!mgr.result.ok || !mgr.result.n) {
                            res.send({
                                status: 'error',
                                msg: 'Database updating error!'
                            });
                        } else {
                            // sending sms with url to recover password
                            var textMessage = 'Recover your password here http://gh3.co/#/' + config.tokenAbbreviation.passwordRecovery + '/' + token + ' by typing in your new password.';
                            sms.send(user._id, user.phoneSMS, textMessage, function(err, utr) {
                                if (err) {
                                    res.send({
                                        status: 'ok',
                                        msg: 'Password recovery was generated and saved into database, but something went wrong with sending text message to ' + user.phoneSMS + ' phone number!',
                                        error: err.error,
                                        data: {
                                            token: token
                                        }
                                    });
                                } else {
                                    res.send({
                                        status: 'ok',
                                        msg: 'Password recovery URL was sent to ' + user.phoneSMS + '!',
                                        data: {
                                            token: token
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

api['auth/check-recovery-token.json'] = function (req, res) {
    var token = req.payload.token;

    if (!token) {
        res.send({
            status: 'error',
            msg: 'Token is missing!'
        });
        return;
    }

    if (token.length !== config.tokenLength) {
        res.send({
            status: 'error',
            msg: 'Token is corrupted!'
        });
        return;
    }

    var users = mongo.db.collection('users');

    users.findOne({
        'passwordRecovery.token': token
    }, function(err, user) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!user) {
            res.send({
                status: 'error',
                msg: 'Password recovery token does not belong to any user.'
            });
        } else {
            res.send({
                status: 'ok',
                msg: 'Token looks okay. Procced with setting a new password!'
            });
        }
    });
};

api['auth/password-set.json'] = function (req, res) {
    var token = req.payload.token;
    var newPassword = req.payload.newPassword;
    var passwordRepeat = req.payload.passwordRepeat;

    if (!token) {
        res.send({
            status: 'error',
            msg: 'Token is missing!'
        });
        return;
    }

    if (token.length !== config.tokenLength) {
        res.send({
            status: 'error',
            msg: 'Token is corrupted!'
        });
        return;
    }

    if (!newPassword) {
        res.send({
            status: 'error',
            msg: 'New password is missing!'
        });
        return;
    }

    if (newPassword.length < config.minPasswordLength) {
        res.send({
            status: 'error',
            msg: 'New password is too short!'
        });
        return;
    }

    if (!passwordRepeat) {
        res.send({
            status: 'error',
            msg: 'Password repeat is missing!'
        });
        return;
    }

    if (passwordRepeat.length < config.minPasswordLength) {
        res.send({
            status: 'error',
            msg: 'Password is too short!'
        });
        return;
    }

    if (newPassword !== passwordRepeat) {
        res.send({
            status: 'error',
            msg: 'New password and password repeat are not the same. They should be!'
        });
        return;
    }

    var users = mongo.db.collection('users');

    users.findOne({
        'passwordRecovery.token': token
    }, {
        _id: 1,
        phone: 1,
        passwordRecovery: 1
    }, function(err, user) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!user) {
            res.send({
                status: 'error',
                msg: 'Password recovery token does not belong to any user.'
            });
        } else {
            var timestamp = new Date().getTime();
            var tokenCreated = user.passwordRecovery.created;
            var elapsedMinutes = Math.floor((timestamp - tokenCreated) / (1000 * 60));

            if (elapsedMinutes >= config.passwordRecoveryValidTime) {
                res.send({
                    status: 'error',
                    msg: 'Password recovery has expired. It was valid only ' + config.passwordRecoveryValidTime + ' minutes.'
                });
                return;
            }

            var hash = require('crypto').createHash('sha256').update(newPassword + secure.salt).digest('hex');

            // updating user
            users.update({
                _id: ObjectId(user._id)
            }, {
                $set: {
                    password: hash
                },
                $unset: {
                    'passwordRecovery': true
                }
            }, function(err, mgr) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!mgr.result.ok || !mgr.result.n) {
                    res.send({
                        status: 'error',
                        msg: 'Database updating error!'
                    });
                } else {
                    res.send({
                        status: 'ok',
                        msg: 'New password is set!',
                        data: {
                            user: user
                        }
                    });
                }
            });
        }
    });
};

api['auth/clown.json'] = function (req, res) {
    var userId = req.payload.userId;

    if (!userId) {
        res.send({
            status: 'error',
            msg: 'User ID is missing!'
        });
        return;
    }

    if (userId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'User ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('users').findOne({
        _id: ObjectId(userId)
    }, function(err, user) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (user) {
            session.set(user._id, user.phone, user.password, req.headers.host, req.headers['user-agent'], function(err, utr) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'We were not able to establish session for you.',
                        error: err.error
                    });
                } else {
                    res.send({
                        status: 'ok',
                        msg: 'Login succeeded!',
                        data: {
                            session: utr.session,
                            userId: user._id
                        }
                    });
                }
            });
        } else {
            res.send({
                status: 'error',
                msg: 'Login did not succeed!'
            });
        }
    });
};

module.exports = api;
