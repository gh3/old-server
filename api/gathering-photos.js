var fs = require('fs');
var lwip = require('lwip');
var exif = require('exif-parser');
var config = require('../core/config.js');
var mongo = require('../core/mongo.js');
var utils = require('../utils/utils.js');
var ObjectId = require('mongodb').ObjectID;
var api = {};

api['gathering-photos/insert.json'] = function (req, res) {
    var gatheringId = req.payload.gatheringId;
    var user = req.user;
    var upload = req.upload;

    if (!gatheringId) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is missing!'
        });
        return;
    }

    if (gatheringId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is corrupted!'
        });
        return;
    }

    if (!Object.keys(upload).length) {
        res.send({
            status: 'error',
            msg: 'No files have been sent. Please try again!'
        });
        return;
    }

    if (Object.keys(upload).length > 1) {
        res.send({
            status: 'error',
            msg: 'Max 1 photo can be uploaded at once!'
        });
        return;
    }

    var newPhoto = {};

    for (var u in upload) {
        newPhoto.fileKey = u;
        newPhoto.size = upload[u].size;
        newPhoto.tmpPath = upload[u].path;
        newPhoto.name = upload[u].name;
        break;
    }

    fs.readFile(newPhoto.tmpPath, function(err, data) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Error at reading photo when checing for file type!'
            });
            return;
        }

        if (data.toString('hex', 0, 4) === config.magicNumbers.png) {
            newPhoto.type = 'image/png';
        } else if (data.toString('hex', 0, 2) === config.magicNumbers.jpg) {
            newPhoto.type = 'image/jpeg';
            newPhoto.exifData = exif.create(data).parse();
        } else {
            res.send({
                status: 'error',
                msg: 'Uploaded file type is not supported!'
            });
            return;
        }

        mongo.db.collection('gatherings').findOne({
            _id: ObjectId(gatheringId),
            isRemoved: {
                $ne: true
            }
        }, function(err, gathering) {
            if (err) {
                res.send({
                    status: 'error',
                    msg: 'Database error!',
                    error: err.message
                });
            } else if (!gathering) {
                res.send({
                    status: 'error',
                    msg: 'Gathering does not exist.'
                });
            } else {
                mongo.db.collection('gatheringParticipants').findOne({
                    gatheringId: ObjectId(gatheringId),
                    userId: ObjectId(user._id),
                }, function(err, gatheringParticipant) {
                    if (err) {
                        res.send({
                            status: 'error',
                            msg: 'Database error!',
                            error: err.message
                        });
                    } else if (!gatheringParticipant) {
                        res.send({
                            status: 'error',
                            msg: 'You do not participate in this gathering!'
                        });
                    } else {

                        newPhoto.userId = ObjectId(user._id);
                        newPhoto.created = new Date().getTime();
                        newPhoto.gatheringId = ObjectId(gathering._id);
                        newPhoto.guid = utils.generateGuid();
                        newPhoto.path = config.folder.gatheringPhotos + newPhoto.guid + '.jpg';

                        if (newPhoto.type === 'image/jpeg') {

                            fs.rename(newPhoto.tmpPath, newPhoto.path, function() {
                                if (err) {
                                    res.send({
                                        status: 'error',
                                        msg: 'Error at moving temporary "jpeg" file to the proper location!'
                                    });
                                } else {
                                    continueWithCropping(newPhoto);
                                }

                            });

                        } else if (newPhoto.type === 'image/png') {

                            lwip.open(newPhoto.tmpPath, function(err, image) {
                                if (err) {
                                    res.send({
                                        status: 'error',
                                        msg: 'Error at opening temporary "png" file and getting ready for converting to "jpeg" type!'
                                    });
                                } else {
                                    image.writeFile(newPhoto.path, function(err){
                                        if (err) {
                                            res.send({
                                                status: 'error',
                                                msg: 'Error at converting "png" type to "jpeg" file and writing to the proper location!'
                                            });
                                        } else {
                                            fs.unlinkSync(newPhoto.tmpPath);
                                            continueWithCropping(newPhoto);
                                        }
                                    });
                                }

                            });
                        }

                        function continueWithCropping(newPhoto) {

                            newPhoto.crops = {
                                '300x300': config.folder.gatheringPhotos + newPhoto.guid + '_300x300.jpg'
                            };

                            lwip.open(newPhoto.path, function(err, image) {
                                if (err) {
                                    res.send({
                                        status: 'error',
                                        msg: 'Error at opening a photo and getting ready for croping!'
                                    });
                                } else {

                                    // https://github.com/EyalAr/lwip/issues/100
                                    if (newPhoto.exifData) {
                                        switch(newPhoto.exifData.tags.Orientation) {
                                            case 2:
                                                image = image.batch().flip('x'); // top-right - flip horizontal
                                                break;
                                            case 3:
                                                image = image.batch().rotate(180); // bottom-right - rotate 180
                                                break;
                                            case 4:
                                                image = image.batch().flip('y'); // bottom-left - flip vertically
                                                break;
                                            case 5:
                                                image = image.batch().rotate(90).flip('x'); // left-top - rotate 90 and flip horizontal
                                                break;
                                            case 6:
                                                image = image.batch().rotate(90); // right-top - rotate 90
                                                break;
                                            case 7:
                                                image = image.batch().rotate(270).flip('x'); // right-bottom - rotate 270 and flip horizontal
                                                break;
                                            case 8:
                                                image = image.batch().rotate(270); // left-bottom - rotate 270
                                                break;
                                            default:
                                                image = image.batch();
                                        }
                                    } else {
                                        image = image.batch();
                                    }

                                    image.cover(300, 300).writeFile(newPhoto.crops['300x300'], function(err) {
                                        if (err) {
                                            res.send({
                                                status: 'error',
                                                msg: 'Error at making a 300x300 photo crop!'
                                            });
                                        } else {
                                            continueWithInsertingToDB(newPhoto);
                                        }
                                    });
                                }
                            });

                            function continueWithInsertingToDB(newPhoto) {
                                mongo.db.collection('gatheringPhotos').insert(newPhoto, function(err, mgr) {
                                    if (err) {
                                        res.send({
                                            status: 'error',
                                            msg: 'Database error!',
                                            error: err.message
                                        });
                                    } else if (!mgr.result.ok || !mgr.result.n) {
                                        res.send({
                                            status: 'error',
                                            msg: 'Database inserting error!'
                                        });
                                    } else {
                                        var gatheringPhoto = mgr.ops[0];
                                        res.send({
                                            status: 'ok',
                                            msg: 'Gathering photos successfully saved!',
                                            data: {
                                                gatheringPhoto: {
                                                    _id: gatheringPhoto._id,
                                                    fileKey: gatheringPhoto.fileKey,
                                                    guid: gatheringPhoto.guid,
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });
    });
};

api['gathering-photos/publish.json'] = function (req, res) {
    var gatheringPhotoId = req.payload.gatheringPhotoId;
    var user = req.user;

    if (!gatheringPhotoId) {
        res.send({
            status: 'error',
            msg: 'Gathering photo ID is missing!'
        });
        return;
    }

    if (gatheringPhotoId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering photo ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatheringPhotos').findOne({
        _id: ObjectId(gatheringPhotoId),
        isRemoved: {
            $ne: true
        }
    }, {
        fileKey: true,
        gatheringId: true,
        guid: true,
        name: true,
        created: true,
        userId: true,
        customName: true,
        type: true
    }, function(err, gatheringPhoto) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gatheringPhoto) {
            res.send({
                status: 'error',
                msg: 'Gathering photo does not exist.'
            });
        } else {

            if (String(gatheringPhoto.userId) !== String(user._id)) {
                res.send({
                    status: 'error',
                    msg: 'You are not the owner of this photo.'
                });
                return;
            }

            if (gatheringPhoto.isPublished) {
                res.send({
                    status: 'error',
                    msg: 'Gathering photo was already published.'
                });
                return;
            }

            mongo.db.collection('gatheringPhotos').update({
                _id: ObjectId(gatheringPhotoId),
                userId: ObjectId(user._id),
                isRemoved: {
                    $ne: true
                }
            }, {
                $set: {
                    isPublished: true,
                    published: new Date().getTime()
                }
            }, function(err, mgr) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!mgr.result.ok || !mgr.result.n) {
                    res.send({
                        status: 'error',
                        msg: 'Database updating error!'
                    });
                } else {
                    res.send({
                        status: 'ok',
                        msg: 'Gathering photo was successfully published!',
                        data: {
                            gatheringPhoto: gatheringPhoto
                        }
                    });
                }
            });
        }
    });
};

api['gathering-photos/find-mine.json'] = function (req, res) {
    var gatheringId = req.payload.gatheringId;
    var user = req.user;

    if (!gatheringId) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is missing!'
        });
        return;
    }

    if (gatheringId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatheringPhotos').find({
        gatheringId: ObjectId(gatheringId),
        userId: ObjectId(user._id),
        isRemoved: {
            $ne: true
        }
    }).toArray(function(err, gatheringPhotos) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else {
            res.send({
                status: 'ok',
                msg: 'Here are your photos!',
                data: {
                    gatheringId: gatheringId,
                    gatheringPhotos: gatheringPhotos
                }
            });
        }
    });
};

api['gathering-photos/find-by-gathering.json'] = function (req, res) {
    var gatheringId = req.payload.gatheringId;
    var user = req.user;

    if (!gatheringId) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is missing!'
        });
        return;
    }

    if (gatheringId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatheringParticipants').findOne({
        gatheringId: ObjectId(gatheringId),
        userId: ObjectId(user._id),
    }, function(err, gatheringParticipant) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gatheringParticipant) {
            res.send({
                status: 'error',
                msg: 'You do not participate in this gathering!'
            });
        } else {
            mongo.db.collection('gatheringPhotos').find({
                gatheringId: ObjectId(gatheringId),
                isPublished: true,
                isRemoved: {
                    $ne: true
                }
            }, {
                fileKey: true,
                gatheringId: true,
                guid: true,
                name: true,
                created: true,
                userId: true,
                published: true,
                customName: true,
                type: true
            }, {
                sort: {
                    published: -1
                }
            }).toArray(function(err, gatheringPhotos) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else {
                    res.send({
                        status: 'ok',
                        msg: 'Here are your photos!',
                        data: {
                            gatheringId: gatheringId,
                            gatheringPhotos: gatheringPhotos
                        }
                    });
                }
            });
        }
    });
};

api['gathering-photos/find-one.json'] = function (req, res) {
    var gatheringPhotoId = req.payload.gatheringPhotoId;
    var user = req.user;

    if (!gatheringPhotoId) {
        res.send({
            status: 'error',
            msg: 'Gathering photo ID is missing!'
        });
        return;
    }

    if (gatheringPhotoId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering photo ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatheringPhotos').findOne({
        _id: ObjectId(gatheringPhotoId),
        isRemoved: {
            $ne: true
        }
    }, function(err, gatheringPhoto) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gatheringPhoto) {
            res.send({
                status: 'error',
                msg: 'Gathering photo does not exist.'
            });
        } else {

            mongo.db.collection('gatheringParticipants').findOne({
                gatheringId: ObjectId(gatheringPhoto.gatheringId),
                userId: ObjectId(user._id),
            }, function(err, gatheringParticipant) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!gatheringParticipant) {
                    res.send({
                        status: 'error',
                        msg: 'You do not participate in this gathering!'
                    });
                } else {
                    res.send({
                        status: 'ok',
                        msg: 'Here is your photo!',
                        data: {
                            gatheringPhoto: gatheringPhoto
                        }
                    });
                }
            });
        }
    });
};

api['gathering-photos/get-one.json'] = function (req, res) {
    var fileName = req.segments[1];
    var user = req.user;

    if (!fileName) {
        res.send({
            status: 'error',
            msg: 'Gathering photo filename is missing.'
        });
    }

    var fileNameSplitted = fileName.split('.');

    if (fileNameSplitted.length > 2) {
        res.send({
            status: 'error',
            msg: 'Gathering photo filename is corrupted.'
        });
    } else if (fileNameSplitted.length < 2) {
        res.send({
            status: 'error',
            msg: 'Gathering photo extension is missing.'
        });
    }

    if (fileNameSplitted[1] !== 'jpg') {
        res.send({
            status: 'error',
            msg: 'Gathering photo extension is wrong.'
        });
    }

    var fileNameSplittedAgain = fileNameSplitted[0].split('_');

    if (fileNameSplittedAgain.length > 2) {
        res.send({
            status: 'error',
            msg: 'Gathering photo filename doesn\'t look okay.'
        });
    } else if (fileNameSplittedAgain.length === 2) {
        var guid = fileNameSplittedAgain[0];
        var crop = fileNameSplittedAgain[1];

        var cropSplitted = crop.split('x');
        if (cropSplitted.length !== 2) {
            res.send({
                status: 'error',
                msg: 'Photo crop doesn\'t look okay.'
            });
        }
    } else {
        var guid = fileNameSplitted[0];
        var crop = false;
    }

    mongo.db.collection('gatheringPhotos').findOne({
        guid: guid,
        isPublished: true,
    }, function(err, gatheringPhoto) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gatheringPhoto) {
            res.send({
                status: 'error',
                msg: 'Gathering photo does not exist.'
            });
        } else {

            mongo.db.collection('gatheringParticipants').findOne({
                gatheringId: ObjectId(gatheringPhoto.gatheringId),
                userId: ObjectId(user._id),
            }, function(err, gatheringParticipant) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!gatheringParticipant) {
                    res.send({
                        status: 'error',
                        msg: 'You do not participate in this gathering!'
                    });
                } else {

                    if (crop) {
                        if (!gatheringPhoto.hasOwnProperty('crops') || !gatheringPhoto.crops.hasOwnProperty(crop)) {
                            res.send({
                                status: 'error',
                                msg: 'Crop doesn\'t exist!'
                            });
                        } else {
                            var location = gatheringPhoto.crops[crop];
                        }
                    } else {
                        var location = gatheringPhoto.path;
                    }

                    // check the existence of requested location
                    fs.stat(location, function(err, fsr) {

                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Photo not found!'
                            });

                        } else if (fsr.isFile()) {

                            // respond with wanted file
                            fs.readFile(location, 'binary', function(err, file) {

                                if (err) {
                                    res.send({
                                        status: 'error',
                                        msg: 'Reading photo binary crashed!'
                                    });
                                } else {
                                    res.send({
                                        file: file,
                                        name: req.path
                                    });
                                }
                            });

                        } else {
                            res.send({
                                status: 'error',
                                msg: 'Requested location is not a file!'
                            });
                        }
                    });
                }
            });
        }
    });
};

api['gathering-photos/remove-to-trash.json'] = function (req, res) {
    var gatheringPhotoId = req.payload.gatheringPhotoId;
    var user = req.user;

    if (!gatheringPhotoId) {
        res.send({
            status: 'error',
            msg: 'Gathering photo ID is missing!'
        });
        return;
    }

    if (gatheringPhotoId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering photo ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatheringPhotos').findOne({
        _id: ObjectId(gatheringPhotoId)
    }, function(err, gatheringPhoto) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gatheringPhoto) {
            res.send({
                status: 'error',
                msg: 'Gathering photo does not exist.'
            });
        } else {

            if (String(gatheringPhoto.userId) !== String(user._id)) {
                res.send({
                    status: 'error',
                    msg: 'You are not the owner of this photo.'
                });
                return;
            }

            if (gatheringPhoto.isRemoved) {
                res.send({
                    status: 'error',
                    msg: 'Gathering photo is already removed to trash.'
                });
                return;
            }

            mongo.db.collection('gatheringPhotos').update({
                _id: ObjectId(gatheringPhotoId),
                userId: ObjectId(user._id),
            }, {
                $set: {
                    isRemoved: true
                }
            }, function(err, mgr) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!mgr.result.ok || !mgr.result.n) {
                    res.send({
                        status: 'error',
                        msg: 'Database updating error!'
                    });
                } else {
                    res.send({
                        status: 'ok',
                        msg: 'Gathering photo was successfuly removed to trash!',
                        data: {
                            gatheringPhotoId: gatheringPhotoId
                        }
                    });
                }
            });
        }
    });
};

api['gathering-photos/remove-to-trash-as-leader.json'] = function (req, res) {
    var gatheringPhotoId = req.payload.gatheringPhotoId;
    var gatheringId = req.payload.gatheringId;
    var user = req.user;

    if (!gatheringId) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is missing!'
        });
        return;
    }

    if (gatheringId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is corrupted!'
        });
        return;
    }

    if (!gatheringPhotoId) {
        res.send({
            status: 'error',
            msg: 'Gathering photo ID is missing!'
        });
        return;
    }

    if (gatheringPhotoId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering photo ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatherings').findOne({
        _id: ObjectId(gatheringId),
        isRemoved: {
            $ne: true
        }
    }, function(err, gathering) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gathering) {
            res.send({
                status: 'error',
                msg: 'Gathering does not exist!'
            });
        } else {
            if (String(user._id) !== String(gathering.userId)) {
                res.send({
                    status: 'error',
                    msg: 'You have to be the leader of this gathering if you want to remove photo to trash!'
                });
                return;
            }

            mongo.db.collection('gatheringPhotos').findOne({
                _id: ObjectId(gatheringPhotoId)
            }, function(err, gatheringPhoto) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!gatheringPhoto) {
                    res.send({
                        status: 'error',
                        msg: 'Gathering photo does not exist.'
                    });
                } else {

                    if (gatheringPhoto.isRemoved) {
                        res.send({
                            status: 'error',
                            msg: 'Gathering photo is already removed to trash.'
                        });
                        return;
                    }

                    mongo.db.collection('gatheringPhotos').update({
                        _id: ObjectId(gatheringPhotoId)
                    }, {
                        $set: {
                            isRemoved: true
                        }
                    }, function(err, mgr) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else if (!mgr.result.ok || !mgr.result.n) {
                            res.send({
                                status: 'error',
                                msg: 'Database updating error!'
                            });
                        } else {
                            res.send({
                                status: 'ok',
                                msg: 'Gathering photo was successfuly removed to trash!',
                                data: {
                                    gatheringPhotoId: gatheringPhotoId
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

api['gathering-photos/remove-one.json'] = function (req, res) {
    var gatheringPhotoId = req.payload.gatheringPhotoId;
    var user = req.user;

    if (!gatheringPhotoId) {
        res.send({
            status: 'error',
            msg: 'Gathering photo ID is missing!'
        });
        return;
    }

    if (gatheringPhotoId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering photo ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatheringPhotos').findOne({
        _id: ObjectId(gatheringPhotoId)
    }, function(err, gatheringPhoto) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gatheringPhoto) {
            res.send({
                status: 'error',
                msg: 'Gathering photo does not exist.'
            });
        } else {

            if (String(gatheringPhoto.userId) !== String(user._id)) {
                res.send({
                    status: 'error',
                    msg: 'You are not the owner of this photo.'
                });
                return;
            }

            if (!gatheringPhoto.isRemoved) {
                res.send({
                    status: 'error',
                    msg: 'Gathering photo needs to be moved to trash first. Then you will be able to permanently remove it.'
                });
                return;
            }

            mongo.db.collection('gatheringPhotos').remove({
                _id: ObjectId(gatheringPhotoId),
                userId: ObjectId(user._id),
            }, function(err, mgr) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!mgr.result.ok || !mgr.result.n) {
                    res.send({
                        status: 'error',
                        msg: 'Database removing error!'
                    });
                } else {

                    try {
                        fs.unlinkSync(gatheringPhoto.path);
                        for (var i in gatheringPhoto.crops) {
                            fs.unlinkSync(gatheringPhoto.crops[i]);
                        }
                    } catch (e) {
                        console.error(e);
                    }

                    res.send({
                        status: 'ok',
                        msg: 'Gathering photo successfully removed!'
                    });
                }
            });
        }
    });
};

api['gathering-photos/update-custom-name.json'] = function (req, res) {
    var customName = req.payload.customName;
    var gatheringPhotoId = req.payload.gatheringPhotoId;
    var user = req.user;

    if (!customName) {
        res.send({
            status: 'error',
            msg: 'Custom name is missing!'
        });
        return;
    }

    if (!gatheringPhotoId) {
        res.send({
            status: 'error',
            msg: 'Gathering photo ID is missing!'
        });
        return;
    }

    if (gatheringPhotoId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering photo ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatheringPhotos').findOne({
        _id: ObjectId(gatheringPhotoId)
    }, function(err, gatheringPhoto) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gatheringPhoto) {
            res.send({
                status: 'error',
                msg: 'Gathering photo does not exist.'
            });
        } else {

            if (String(gatheringPhoto.userId) !== String(user._id)) {
                res.send({
                    status: 'error',
                    msg: 'You are not the owner of this photo.'
                });
                return;
            }

            if (gatheringPhoto.isRemoved) {
                res.send({
                    status: 'error',
                    msg: 'Gathering photo is already removed to trash.'
                });
                return;
            }

            mongo.db.collection('gatheringPhotos').update({
                _id: ObjectId(gatheringPhotoId),
                userId: ObjectId(user._id),
            }, {
                $set: {
                    customName: customName
                }
            }, function(err, mgr) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!mgr.result.ok || !mgr.result.n) {
                    res.send({
                        status: 'error',
                        msg: 'Database updating error!'
                    });
                } else {
                    res.send({
                        status: 'ok',
                        msg: 'Gathering photo\'s custom name was successfuly updated!',
                        data: {
                            gatheringPhotoId: gatheringPhotoId,
                            customName: customName
                        }
                    });
                }
            });
        }
    });
};

api['gathering-photos/find-removed.json'] = function (req, res) {
    var user = req.user;

    mongo.db.collection('gatheringPhotos').find({
        userId: ObjectId(user._id),
        isRemoved: true
    }, {
        fileKey: true,
        gatheringId: true,
        guid: true,
        name: true,
        created: true,
        userId: true,
        published: true,
        customName: true,
        type: true
    }).toArray(function(err, gatheringPhotos) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else {
            res.send({
                status: 'ok',
                msg: 'Here are your removed photos!',
                data: {
                    gatheringPhotos: gatheringPhotos
                }
            });
        }
    });
};

module.exports = api;
