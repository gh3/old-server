var mongo = require('../core/mongo.js');
var ObjectId = require('mongodb').ObjectID;
var api = {};

api['cron/mark-gatherings-as-removed.json'] = function (req, res) {
    if (req.headers.host !== '127.0.0.1') {
        res.send({
            status: 'error',
            msg: 'Sorry pal, cron works locally only!'
        });
        return;
    }

    mongo.db.collection('gatherings').find({
        removingDate: {
            $lte: new Date().getTime()
        }, isRemoved: {
            $ne: true
        }
    }, {
        _id: true
    }).toArray(function(err, gatherings) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else {

            var gatheringsIds = gatherings.map(elm => ObjectId(elm._id));

            if (!gatheringsIds.length) {
                res.send({
                    status: 'ok',
                    msg: 'No gatherings to remove at the moment!'
                });
            }

            mongo.db.collection('gatherings').update({
                _id: {
                    $in: gatheringsIds
                }
            }, {
                $set: {
                    isRemoved: true
                }
            }, {
                multi: true
            }, function(err, mgr) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!mgr.result.ok) {
                    res.send({
                        status: 'error',
                        msg: 'Database updating error!'
                    });
                } else {
                    var removedGatheringsCount = mgr.result.n;

                    mongo.db.collection('gatheringPhotos').update({
                        gatheringId: {
                            $in: gatheringsIds
                        }
                    }, {
                        $set: {
                            isRemoved: true
                        }
                    }, {
                        multi: true
                    }, function(err, mgr) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else if (!mgr.result.ok) {
                            res.send({
                                status: 'error',
                                msg: 'Database updating error!'
                            });
                        } else {
                            var removedGatheringPhotosCount = mgr.result.n;

                            res.send({
                                status: 'ok',
                                msg: 'Succeeded!',
                                data: {
                                    removedGatheringsIds: gatheringsIds,
                                    removedGatheringsCount: removedGatheringsCount,
                                    removedGatheringPhotosCount: removedGatheringPhotosCount
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

module.exports = api;
