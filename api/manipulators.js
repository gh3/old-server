var fs = require('fs');
var lwip = require('lwip');
var config = require('../core/config.js');
var mongo = require('../core/mongo.js');
var utils = require('../utils/utils.js');
var ObjectId = require('mongodb').ObjectID;
var api = {};

api['manipulators/set-guid.json'] = function (req, res) {
    mongo.db.collection('gatheringPhotos').find({
    }).toArray(function(err, gatheringPhotos) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else {
            var errors = [];
            for (var i in gatheringPhotos) {
                if (!gatheringPhotos[i].hasOwnProperty('guid')) {
                    mongo.db.collection('gatheringPhotos').update({
                        _id: ObjectId(gatheringPhotos[i]._id)
                    }, {
                        $set: {
                            guid: utils.generateGuid()
                        }
                    }, function(err, mgr) {
                        if (err) {
                            errors.push({
                                gatheringPhoto: gatheringPhotos[i]._id,
                                error: err
                            });
                        }
                    });
                }
            }
            res.send({
                status: 'ok',
                msg: 'Might be okay...',
                errors: errors
            });
        }
    });
};

api['manipulators/rename-photos-accordingly-to-guid.json'] = function (req, res) {
    mongo.db.collection('gatheringPhotos').find({
    }).toArray(function(err, gatheringPhotos) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else {
            for (var i in gatheringPhotos) {
                if (gatheringPhotos[i].hasOwnProperty('path') && gatheringPhotos[i].hasOwnProperty('guid') && gatheringPhotos[i].hasOwnProperty('generatedName')) {

                    var oldPath = config.folder.gatheringPhotos + gatheringPhotos[i].generatedName;
                    var newPath = config.folder.gatheringPhotos + gatheringPhotos[i].guid + '.jpg';

                    try {
                        fs.renameSync(oldPath, newPath);
                    } catch (e) {
                        console.error(e);
                    }

                    mongo.db.collection('gatheringPhotos').update({
                        _id: ObjectId(gatheringPhotos[i]._id)
                    }, {
                        $set: {
                            path: newPath
                        }
                    }, function(err, mgr) {
                        if (err) {
                            console.error({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        }
                    });
                }
            }
            res.send({
                status: 'ok',
                msg: 'Might be okay, check console (or log) for errors...'
            });
        }
    });
}

api['manipulators/check-if-any-png.json'] = function (req, res) {
    var errors = [];

    fs.readdirSync(config.folder.gatheringPhotos).forEach(function(file) {
        fs.readFile(config.folder.gatheringPhotos + file, function(err, code) {
            if (err) {
                res.send({
                    status: 'error',
                    msg: 'Error at reading photo when checking for file type!',
                    data: {
                        file: gatheringPhotos[i].path
                    }
                });
                return;
            }
            if (code.toString('hex', 0, 4) === config.magicNumbers.png) {
                res.send({
                    status: 'error',
                    msg: 'There is png which is not!',
                    data: {
                        file: file
                    }
                });
            }
        });
    });
}

api['manipulators/convert-from-png-to-jpeg.json'] = function (req, res) {
    var fileName = req.payload.fileName;

    if (!fileName) {
        res.send({
            status: 'error',
            msg: 'Gathering photo filename is missing.'
        });
    }

    var fileNameSplitted = fileName.split('.');

    if (fileNameSplitted.length > 2) {
        res.send({
            status: 'error',
            msg: 'Gathering photo filename is corrupted.'
        });
    } else if (fileNameSplitted.length < 2) {
        res.send({
            status: 'error',
            msg: 'Gathering photo extension is missing.'
        });
    }

    fs.readFile(config.folder.gatheringPhotos + fileName, function(err, code) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Error at reading photo! It looks like it doesn\'t exist!'
            });
            return;
        }

        if (code.toString('hex', 0, 4) !== config.magicNumbers.png) {
            res.send({
                status: 'error',
                msg: 'Photo is not a "png" type!'
            });
        }

        lwip.open(config.folder.gatheringPhotos + fileName, function(err, image) {
            if (err) {
                res.send({
                    status: 'error',
                    msg: 'Error at opening "png" file and getting ready for converting to "jpeg" type!'
                });
            } else {
                image.writeFile(config.folder.gatheringPhotos + fileNameSplitted[0] + '.jpg', function(err){
                    if (err) {
                        res.send({
                            status: 'error',
                            msg: 'Error at converting "png" type to "jpeg" file and writing to the proper location!'
                        });
                    } else {
                        fs.unlinkSync(config.folder.gatheringPhotos + fileName);
                        res.send({
                            status: 'ok',
                            msg: 'It seems okay!'
                        });
                    }
                });
            }
        });
    });
}

api['manipulators/create-300x300-crops.json'] = function (req, res) {
    var gatheringPhotoId = req.payload.gatheringPhotoId;

    if (!gatheringPhotoId) {
        res.send({
            status: 'error',
            msg: 'Gathering photo ID is missing!'
        });
        return;
    }
    
    mongo.db.collection('gatheringPhotos').find({
        _id: ObjectId(gatheringPhotoId)
    }).toArray(function(err, gatheringPhotos) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else {
            for (var i in gatheringPhotos) {
                if (!gatheringPhotos[i].hasOwnProperty('guid')) {
                    console.error({
                        status: 'error',
                        msg: 'Photo has no guid.',
                        data: {
                            gatheringPhotoId: gatheringPhotos[i]._id
                        }
                    });
                }

                if (!gatheringPhotos[i].hasOwnProperty('crops') || gatheringPhotos[i].hasOwnProperty('crops') && !gatheringPhotos[i].crops.hasOwnProperty('300x300')) {
                    
                    var cropPath =  config.folder.gatheringPhotos + gatheringPhotos[i].guid + '_300x300.jpg';

                    lwip.open(gatheringPhotos[i].path, function(err, image) {
                        if (err) {
                            console.error({
                                status: 'error',
                                msg: 'Error at opening a photo and getting ready for croping!',
                                data: {
                                    gatheringPhotoId: gatheringPhotos[i]._id
                                }
                            });
                        } else {
                            image.batch().cover(300, 300).writeFile(cropPath, function(err) {
                                if (err) {
                                    console.error({
                                        status: 'error',
                                        msg: 'Error at making a 300x300 photo crop!',
                                        data: {
                                            gatheringPhotoId: gatheringPhotos[i]._id
                                        }
                                    });
                                } else {
                                    mongo.db.collection('gatheringPhotos').update({
                                        _id: ObjectId(gatheringPhotos[i]._id)
                                    }, {
                                        $set: {
                                            crops: {
                                                '300x300': cropPath
                                            }
                                        }
                                    }, function(err, mgr) {
                                        if (err) {
                                            console.error({
                                                status: 'error',
                                                msg: 'Database error!',
                                                error: err.message
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
            res.send({
                status: 'ok',
                msg: 'Might be okay, check console (or log) for errors...'
            });
        }
    });
}

module.exports = api;
