var config = require('../core/config.js');
var mongo = require('../core/mongo.js');
var ObjectId = require('mongodb').ObjectID;
var api = {};

api['gatherings/insert.json'] = function (req, res) {
    var user = req.user;
    var title = req.payload.title;

    if (!title) {
        res.send({
            status: 'error',
            msg: 'Title is missing!'
        });
        return;
    }

    if (title.length == 0) {
        res.send({
            status: 'error',
            msg: 'Enter title!'
        });
        return;
    }

    mongo.db.collection('gatherings').insert({
        title: title,
        userId: ObjectId(user._id),
        created: new Date().getTime()
    }, function(err, mgr) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!mgr.result.ok || !mgr.result.n) {
            res.send({
                status: 'error',
                msg: 'Database inserting error!'
            });
        } else {
            var newGathering = mgr.ops[0];
            var newGatheringParticipant = {
                userId: ObjectId(user._id),
                gatheringId: ObjectId(mgr.insertedIds[0]),
                creatorId: ObjectId(user._id),
                created: new Date().getTime(),
            };
            mongo.db.collection('gatheringParticipants').insert(newGatheringParticipant, function(err, mgr) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!mgr.result.ok || !mgr.result.n) {
                    res.send({
                        status: 'error',
                        msg: 'Database inserting error!'
                    });
                } else {
                    res.send({
                        status: 'ok',
                        msg: 'New gathering created! You have beed added as a leader as well as participant to it!',
                        data: {
                            gathering: newGathering
                        }
                    });
                }
            });
            
        }
    });
};

api['gatherings/find-where-leader.json'] = function (req, res) {
    var user = req.user;

    mongo.db.collection('gatherings').find({
        userId: ObjectId(user._id),
        isRemoved: {
            $ne: true
        }
    }, {
        _id: true,
        title: true,
        userId: true,
        created: true,
        removingDate: true
    }).toArray(function(err, gatherings) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else {
            res.send({
                status: 'ok',
                msg: 'Here is the list of gatherings where you are the leader!',
                data: {
                    gatherings: gatherings
                }
            });
        }
    });
};

api['gatherings/find-where-participate.json'] = function (req, res) {
    var user = req.user;

    mongo.db.collection('gatheringParticipants').find({
        userId: ObjectId(user._id)
    }).toArray(function(err, gatheringParticipant) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else {
            var gatheringsIds = gatheringParticipant.map(elm => ObjectId(elm.gatheringId));

            if (!gatheringsIds.length) {
                res.send({
                    status: 'ok',
                    msg: 'Here is the list of gatherings where you participate!',
                    data: {
                        gatherings: []
                    }
                });
                return;
            }

            mongo.db.collection('gatherings').find({
                _id: {
                    $in: gatheringsIds
                },
                isRemoved: {
                    $ne: true
                }
            }, {
                _id: true,
                title: true,
                userId: true,
                created: true,
                removingDate: true
            }).toArray(function(err, gatherings) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else {
                    res.send({
                        status: 'ok',
                        msg: 'Here is the list of gatherings where you participate!',
                        data: {
                            gatherings: gatherings
                        }
                    });
                }
            });
        }
    });
};

api['gatherings/prepare-for-removing.json'] = function (req, res) {
    var gatheringId = req.payload.gatheringId;
    var user = req.user;

    if (!gatheringId) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is missing!'
        });
        return;
    }

    if (gatheringId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatherings').findOne({
        _id: ObjectId(gatheringId),
        isRemoved: {
            $ne: true
        }
    }, function(err, gathering) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gathering) {
            res.send({
                status: 'error',
                msg: 'Gathering does not exist!'
            });
        } else {
            if (String(user._id) !== String(gathering.userId)) {
                res.send({
                    status: 'error',
                    msg: 'You have to be the leader of this gathering if you want to remove it!'
                });
                return;
            }

            if (gathering.hasOwnProperty('removingDate')) {
                res.send({
                    status: 'error',
                    msg: 'This gathering is already set for removing!'
                });
                return;
            }

            var removingDate = new Date();
            removingDate = removingDate.setDate(removingDate.getDate() + config.gatheringRemovingPeriod);

            mongo.db.collection('gatherings').update({
                _id: ObjectId(gatheringId)
            }, {
                $set: {
                    removingDate: removingDate
                }
            }, function(err, mgr) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!mgr.result.ok || !mgr.result.n) {
                    res.send({
                        status: 'error',
                        msg: 'Database updating error!'
                    });
                } else {
                    res.send({
                        status: 'ok',
                        msg: 'Removing was successfully set! Gathering will be removed soon.',
                        data: {
                            removingDate: removingDate
                        }
                    });
                }
            });
        }
    });
};

api['gatherings/cancel-removing.json'] = function (req, res) {
    var gatheringId = req.payload.gatheringId;
    var user = req.user;

    if (!gatheringId) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is missing!'
        });
        return;
    }

    if (gatheringId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatherings').findOne({
        _id: ObjectId(gatheringId),
        isRemoved: {
            $ne: true
        }
    }, function(err, gathering) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gathering) {
            res.send({
                status: 'error',
                msg: 'Gathering does not exist!'
            });
        } else {
            if (String(user._id) !== String(gathering.userId)) {
                res.send({
                    status: 'error',
                    msg: 'You have to be the leader of this gathering if you want to remove it!'
                });
                return;
            }

            if (!gathering.hasOwnProperty('removingDate')) {
                res.send({
                    status: 'error',
                    msg: 'This gathering is not set for removing yet!'
                });
                return;
            }

            mongo.db.collection('gatherings').update({
                _id: ObjectId(gatheringId)
            }, {
                $unset: {
                    removingDate: true
                }
            }, function(err, mgr) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!mgr.result.ok || !mgr.result.n) {
                    res.send({
                        status: 'error',
                        msg: 'Database updating error!'
                    });
                } else {
                    res.send({
                        status: 'ok',
                        msg: 'Removing was successfully canceled! Gathering won\'t be removed.'
                    });
                }
            });
        }
    });
};

api['gatherings/remove.json'] = function (req, res) {
    res.send({
        status: 'error',
        msg: 'Not in use at the moment!'
    });
    return;

    var gatheringId = req.payload.gatheringId;
    var user = req.user;

    if (!gatheringId) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is missing!'
        });
        return;
    }

    if (gatheringId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatherings').findOne({
        _id: ObjectId(gatheringId),
        isRemoved: {
            $ne: true
        }
    }, function(err, gathering) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gathering) {
            res.send({
                status: 'error',
                msg: 'Gathering does not exist!'
            });
        } else {
            if (String(user._id) !== String(gathering.userId)) {
                res.send({
                    status: 'error',
                    msg: 'You have to be the leader of this gathering if you want to remove it!'
                });
                return;
            }

            if (!gathering.hasOwnProperty('removingDate')) {
                res.send({
                    status: 'error',
                    msg: 'This gathering can not be removed. Removing data is not set yet!'
                });
                return;
            }

            var currentTime = new Date().getTime();
            if (currentTime < gathering.removingDate) {
                res.send({
                    status: 'error',
                    msg: 'This gathering can not be removed at the moment. Removing data has not passed yet!'
                });
                return;
            }

            mongo.db.collection('gatherings').remove({
                _id: ObjectId(gatheringId),
                userId: ObjectId(user._id)
            }, function(err, mgr) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!mgr.result.ok || !mgr.result.n) {
                    res.send({
                        status: 'error',
                        msg: 'Database removing error!'
                    });
                } else {
                    mongo.db.collection('gatheringParticipants').remove({
                        gatheringId: ObjectId(gatheringId)
                    }, function(err, mgr) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else if (!mgr.result.ok || !mgr.result.n) {
                            res.send({
                                status: 'error',
                                msg: 'Database removing error!'
                            });
                        } else {
                            res.send({
                                status: 'ok',
                                msg: 'Gathering and its participants successfully removed!'
                            });
                        }
                    });
                }
            });
        }
    });
};

api['gatherings/find-one.json'] = function (req, res) {
    var gatheringId = req.payload.gatheringId;
    var user = req.user;

    if (!gatheringId) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is missing!'
        });
        return;
    }

    if (gatheringId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatherings').findOne({
        _id: ObjectId(gatheringId),
        isRemoved: {
            $ne: true
        }
    }, {
        _id: true,
        title: true,
        userId: true,
        created: true,
        removingDate: true
    }, function(err, gathering) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gathering) {
            res.send({
                status: 'error',
                msg: 'Gathering does not exist!'
            });
        } else {
            if (String(user._id) === String(gathering.userId)) {
                gathering['leader'] = true;
                res.send({
                    status: 'ok',
                    msg: 'Here is your gathering!',
                    data: {
                        gathering: gathering
                    }
                });
                return;
            }

            mongo.db.collection('gatheringParticipants').findOne({
                userId: ObjectId(user._id),
                gatheringId: ObjectId(gatheringId)
            }, function(err, gatheringParticipant) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (gatheringParticipant) {
                    gathering['leader'] = false;
                    res.send({
                        status: 'ok',
                        msg: 'Here is your gathering!',
                        data: {
                            gathering: gathering
                        }
                    });
                } else {
                    res.send({
                        status: 'error',
                        msg: 'You do not participate in this gathering!'
                    });
                }
            });
        }
    });
};

module.exports = api;
