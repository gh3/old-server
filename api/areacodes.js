var config = require('../core/config.js');
var mongo = require('../core/mongo.js');
var api = {};

api['areacodes/find.json'] = function (req, res) {

    mongo.db.collection('areacodes').find().toArray(function(err, codes) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else {
            res.send({
                status: 'ok',
                msg: 'Here is the list of area codes!',
                data: {
                    areacodes: codes
                }
            });
        }
    });
};

module.exports = api;
