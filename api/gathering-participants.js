var config = require('../core/config.js');
var mongo = require('../core/mongo.js');
var ObjectId = require('mongodb').ObjectID;
var api = {};

api['gathering-participants/add-user.json'] = function (req, res) {
    var gatheringId = req.payload.gatheringId;
    var userId = req.payload.userId;

    if (!gatheringId) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is missing!'
        });
        return;
    }

    if (gatheringId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is corrupted!'
        });
        return;
    }

    if (!userId) {
        res.send({
            status: 'error',
            msg: 'User ID is missing!'
        });
        return;
    }

    if (userId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'User ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatherings').findOne({
        _id: ObjectId(gatheringId),
        isRemoved: {
            $ne: true
        }
    }, function(err, gathering) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gathering) {
            res.send({
                status: 'error',
                msg: 'Gathering does not exist.'
            });
        } else {
            if (String(gathering.userId) !== String(req.user._id)) {
                res.send({
                    status: 'error',
                    msg: 'You have to be the leader of gathering if you want to add participant to it!'
                });
                return;
            }

            mongo.db.collection('users').findOne({
                _id: ObjectId(userId)
            }, function(err, user) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!user) {
                    res.send({
                        status: 'error',
                        msg: 'User does not exist.'
                    });
                } else {
                    mongo.db.collection('gatheringParticipants').findOne({
                        gatheringId: ObjectId(gatheringId),
                        userId: ObjectId(userId)
                    }, function(err, gatheringParticipant) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else if (gatheringParticipant) {
                            res.send({
                                status: 'error',
                                msg: 'Selected user already exists in this gathering.'
                            });
                        } else {
                            var newGatheringParticipant = {
                                userId: ObjectId(userId),
                                gatheringId: ObjectId(gatheringId),
                                creatorId: ObjectId(req.user._id),
                                created: new Date().getTime()
                            };
                            mongo.db.collection('gatheringParticipants').insert(newGatheringParticipant, function(err, mgr) {
                                if (err) {
                                    res.send({
                                        status: 'error',
                                        msg: 'Database error!',
                                        error: err.message
                                    });
                                } else if (!mgr.result.ok || !mgr.result.n) {
                                    res.send({
                                        status: 'error',
                                        msg: 'Database inserting error!'
                                    });
                                } else {
                                    res.send({
                                        status: 'ok',
                                        msg: 'User successfully added to the gathering!',
                                        data: {
                                            gatheringParticipant: mgr.ops[0]
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

api['gathering-participants/find-by-gathering.json'] = function (req, res) {
    var gatheringId = req.payload.gatheringId;
    var user = req.user;

    if (!gatheringId) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is missing!'
        });
        return;
    }

    if (gatheringId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatherings').findOne({
        _id: ObjectId(gatheringId),
        isRemoved: {
            $ne: true
        }
    }, function(err, gathering) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gathering) {
            res.send({
                status: 'error',
                msg: 'Gathering does not exist.'
            });
        } else {
            mongo.db.collection('gatheringParticipants').find({
                gatheringId: ObjectId(gatheringId)
            }).toArray(function(err, gatheringParticipants) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else {
                    var userIds = gatheringParticipants.map(elm => String(elm.userId));

                    if (!userIds.includes(String(user._id)) && String(gathering.userId) !== String(user._id)) {
                        res.send({
                            status: 'error',
                            msg: 'You are not in this gathering neither the leader of it!'
                        });
                        return;
                    }

                    userIds = gatheringParticipants.map(elm => ObjectId(elm.userId));

                    mongo.db.collection('users').find({
                        _id: {
                            $in: userIds
                        }
                    }, {
                        name: true,
                    }).toArray(function(err, users) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else {
                            res.send({
                                status: 'ok',
                                msg: 'Here are participants of the gathering!',
                                data: {
                                    gatheringParticipants: users
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

api['gathering-participants/remove-user.json'] = function (req, res) {
    var gatheringId = req.payload.gatheringId;
    var userId = req.payload.userId;

    if (!gatheringId) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is missing!'
        });
        return;
    }

    if (gatheringId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is corrupted!'
        });
        return;
    }

    if (!userId) {
        res.send({
            status: 'error',
            msg: 'User ID is missing!'
        });
        return;
    }

    if (userId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'User ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatherings').findOne({
        _id: ObjectId(gatheringId),
        isRemoved: {
            $ne: true
        }
    }, function(err, gathering) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gathering) {
            res.send({
                status: 'error',
                msg: 'Gathering does not exist.'
            });
        } else {
            if (String(gathering.userId) !== String(req.user._id)) {
                res.send({
                    status: 'error',
                    msg: 'You have to be the leader of gathering if you want to remove participant from it!'
                });
                return;
            }

            if (String(gathering.userId) === String(userId)) {
                res.send({
                    status: 'error',
                    msg: 'You can not be removed from this gathering because you are the leader of it!'
                });
                return;
            }

            mongo.db.collection('gatheringParticipants').findOne({
                gatheringId: ObjectId(gatheringId),
                userId: ObjectId(userId)
            }, function(err, gatheringParticipant) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!gatheringParticipant) {
                    res.send({
                        status: 'error',
                        msg: 'User does not participate in this gathering!'
                    });
                } else {
                    mongo.db.collection('gatheringParticipants').remove({
                        gatheringId: ObjectId(gatheringId),
                        userId: ObjectId(userId)
                    }, function(err, mgr) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else if (!mgr.result.ok || !mgr.result.n) {
                            res.send({
                                status: 'error',
                                msg: 'Database removing error!'
                            });
                        } else {
                            res.send({
                                status: 'ok',
                                msg: 'User successfully removed from gathering!'
                            });
                        }
                    });
                }
            });
        }
    });
};

api['gathering-participants/remove-me.json'] = function (req, res) {
    var gatheringId = req.payload.gatheringId;
    var user = req.user;

    if (!gatheringId) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is missing!'
        });
        return;
    }

    if (gatheringId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatherings').findOne({
        _id: ObjectId(gatheringId),
        isRemoved: {
            $ne: true
        }
    }, function(err, gathering) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gathering) {
            res.send({
                status: 'error',
                msg: 'Gathering does not exist.'
            });
        } else {
            if (String(gathering.userId) === String(user._id)) {
                res.send({
                    status: 'error',
                    msg: 'You can not remove yourself from this gathering because you are the leader of it!'
                });
                return;
            }

            mongo.db.collection('gatheringParticipants').findOne({
                gatheringId: ObjectId(gatheringId),
                userId: ObjectId(user._id)
            }, function(err, gatheringParticipant) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!gatheringParticipant) {
                    res.send({
                        status: 'error',
                        msg: 'You do not participate in this gathering!'
                    });
                } else {
                    mongo.db.collection('gatheringParticipants').remove({
                        gatheringId: ObjectId(gatheringId),
                        userId: ObjectId(user._id)
                    }, function(err, mgr) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else if (!mgr.result.ok || !mgr.result.n) {
                            res.send({
                                status: 'error',
                                msg: 'Database removing error!'
                            });
                        } else {
                            res.send({
                                status: 'ok',
                                msg: 'You were successfully removed from gathering!'
                            });
                        }
                    });
                }
            });
        }
    });
};

module.exports = api;
