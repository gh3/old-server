var config = require('../core/config.js');
var sms = require('../utils/sms.js');
var mongo = require('../core/mongo.js');
var api = {};

api['other/public-files.json'] = function (req, res) {
    var exec = require('child_process').exec;
    exec('find ../client/public', {
        timeout: 1000,
        maxBuffer: 100 * 1024
    }, function(error, stdout, stderr) {
        res.send({
            status: 'ok',
            data: {
                files: stdout.replace(/^\s+|\s+$/g, '').split('\n')
            }
        });
    });
};

api['other/api-calls.json'] = function (req, res) {
    res.send({
        status: 'ok',
        data: config.api
    });
};

api['other/send-sms.json'] = function (req, res) {
    var user = req.user;
    var phone = req.payload.phone;
    var text = req.payload.text;
    var areacode = req.payload.areacode;

    if (!areacode) {
        res.send({
            status: 'error',
            msg: 'You have to select your area!'
        });
        return;
    }

    // convert double zero into plus
    if (areacode.substring(0,2) === '00') {
        areacode = '+' + areacode.slice(2);
    }

    // starting with plus sign
    if (areacode.charAt(0) !== '+') {
        res.send({
            status: 'error',
            msg: 'Area code should start with plus sign!'
        });
        return;
    }

    if (areacode.length < 2) {
        res.send({
            status: 'error',
            msg: 'Area code is too short!'
        });
        return;
    }

    if (!phone) {
        res.send({
            status: 'error',
            msg: 'Phone is missing!'
        });
        return;
    }

    // convert international phone number to local
    phone = phone.replace(areacode, 0);

    // starting with digit zero
    if (phone.charAt(0) !== '0') {
        res.send({
            status: 'error',
            msg: 'Phone should start with digit zero!'
        });
        return;
    }

    // only digits
    var isNum = true;
    isNum = /^[\d+]+$/.test(phone);
    if (!isNum) {
        res.send({
            status: 'error',
            msg: 'Phone should contain only digits.'
        });
        return;
    }

    if (phone.length < config.minPhoneLength) {
        res.send({
            status: 'error',
            msg: 'Phone is too short!'
        });
        return;
    }

    if (!text) {
        res.send({
            status: 'error',
            msg: 'Enter text!'
        });
        return;
    }

    mongo.db.collection('areacodes').findOne({
        areacode: areacode
    }, function(err, area) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!area) {
            res.send({
                status: 'error',
                msg: 'We sadly do not support area ' + areacode + ' yet!'
            });
        } else {
            // phone for SMS
            var phoneSMS = area.areacode + phone.substr(1);

            sms.send(user._id, phoneSMS, text, function(err, utr) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Error while sending text!',
                        error: err.error
                    });
                } else {
                    res.send({
                        status: 'ok',
                        msg: 'Text is on its way to hit the receiver!'
                    });
                }
            });
        }
    });
};

api['other/upload-something.json'] = function (req, res) {
    res.send({
        status: 'ok',
        data: {
            files: req.upload,
            payload: req.payload
        }
    });
};

module.exports = api;
