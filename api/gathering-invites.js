var config = require('../core/config.js');
var secure = require('../core/secure.js');
var sms = require('../utils/sms.js');
var session = require('../utils/session.js');
var mongo = require('../core/mongo.js');
var ObjectId = require('mongodb').ObjectID;
var api = {};

api['gathering-invites/send-via-sms.json'] = function (req, res) {
    var gatheringId = req.payload.gatheringId;
    var phone = req.payload.phone;
    var areacode = req.payload.areacode;
    var user = req.user;

    if (!user.hasOwnProperty('name') || user.name === '') {
        res.send({
            status: 'error',
            msg: 'You have not set you name yet! You need to do it before sending invites.'
        });
        return;
    }

    if (!gatheringId) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is missing!'
        });
        return;
    }

    if (gatheringId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is corrupted!'
        });
        return;
    }

    if (!areacode) {
        res.send({
            status: 'error',
            msg: 'You have to select your area!'
        });
        return;
    }

    // convert double zero into plus
    if (areacode.substring(0,2) === '00') {
        areacode = '+' + areacode.slice(2);
    }

    // starting with plus sign
    if (areacode.charAt(0) !== '+') {
        res.send({
            status: 'error',
            msg: 'Area code should start with plus sign!'
        });
        return;
    }

    if (areacode.length < 2) {
        res.send({
            status: 'error',
            msg: 'Area code is too short!'
        });
        return;
    }

    if (!phone) {
        res.send({
            status: 'error',
            msg: 'Phone is missing!'
        });
        return;
    }

    // convert international phone number to local
    phone = phone.replace(areacode, 0);

    // starting with digit zero
    if (phone.charAt(0) !== '0') {
        res.send({
            status: 'error',
            msg: 'Phone should start with digit zero!'
        });
        return;
    }

    // only digits
    var isNum = true;
    isNum = /^[\d+]+$/.test(phone);
    if (!isNum) {
        res.send({
            status: 'error',
            msg: 'Phone should contain only digits.'
        });
        return;
    }

    if (phone.length < config.minPhoneLength) {
        res.send({
            status: 'error',
            msg: 'Phone is too short!'
        });
        return;
    }

    mongo.db.collection('areacodes').findOne({
        areacode: areacode
    }, function(err, area) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!area) {
            res.send({
                status: 'error',
                msg: 'We sadly do not support area ' + areacode + ' yet!'
            });
        } else {
            // phone for SMS
            var phoneSMS = area.areacode + phone.substr(1);

            mongo.db.collection('gatherings').findOne({
                _id: ObjectId(gatheringId),
                isRemoved: {
                    $ne: true
                }
            }, function(err, gathering) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!gathering) {
                    res.send({
                        status: 'error',
                        msg: 'Gathering does not exist.'
                    });
                } else {
                    if (!gathering.title) {
                        res.send({
                            status: 'error',
                            msg: 'Gathering\'s title has not been set yet. You need to do it before sending invites!'
                        });
                        return;
                    }

                    if (String(gathering.userId) !== String(user._id)) {
                        res.send({
                            status: 'error',
                            msg: 'You have to be the leader of gathering if you want to invite participant to it!'
                        });
                        return;
                    }

                    mongo.db.collection('gatheringParticipants').find({
                        gatheringId: ObjectId(gatheringId)
                    }).toArray(function(err, gatheringParticipant) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else {

                            var userIds = gatheringParticipant.map(elm => ObjectId(elm.userId));

                            mongo.db.collection('users').find({
                                _id: {
                                    $in: userIds
                                }
                            }, {
                                phoneSMS: true
                            }).toArray(function(err, users) {
                                if (err) {
                                    res.send({
                                        status: 'error',
                                        msg: 'Database error!',
                                        error: err.message
                                    });
                                } else {
                                    var phoneSMSs = users.map(elm => elm.phoneSMS);

                                    if (phoneSMSs.includes(phoneSMS)) {
                                        res.send({
                                            status: 'error',
                                            msg: 'User with phone number ' + phoneSMS + ' is already participating in this gathering!'
                                        });
                                        return;  
                                    }

                                    mongo.db.collection('gatheringInvites').findOne({
                                        gatheringId: ObjectId(gatheringId),
                                        phoneSMS: phoneSMS
                                    }, function(err, gatheringInvite) {
                                        if (err) {
                                            res.send({
                                                status: 'error',
                                                msg: 'Database error!',
                                                error: err.message
                                            });
                                        } else if (gatheringInvite) {
                                            res.send({
                                                status: 'error',
                                                msg: 'Phone number ' + phoneSMS + ' was already invited to this gathering!'
                                            });
                                        } else {

                                            // generating random token from readable letters
                                            var token = '';
                                            for (var i = 0; i < config.tokenLength; i++) {
                                                token += config.readableLetters.charAt(Math.floor(Math.random() * config.readableLetters.length));
                                            }

                                            mongo.db.collection('gatheringInvites').insert({
                                                created: new Date().getTime(),
                                                createdBy: ObjectId(user._id),
                                                gatheringId: ObjectId(gatheringId),
                                                phoneSMS: phoneSMS,
                                                phone: phone,
                                                areacodeId: ObjectId(area._id),
                                                token: token
                                            }, function(err, mgr) {
                                                if (err) {
                                                    res.send({
                                                        status: 'error',
                                                        msg: 'Database error!',
                                                        error: err.message
                                                    });
                                                } else if (!mgr.result.ok || !mgr.result.n) {
                                                    res.send({
                                                        status: 'error',
                                                        msg: 'Database inserting error!'
                                                    });
                                                } else {
                                                    var textMessage = user.name + ' invites you to ' + gathering.title + '. Open http://gh3.co/#/' + config.tokenAbbreviation.gatheringInvite + '/' + token + ' and join now.';
                                                    if (textMessage.length > config.maxLengthOfSMS) {
                                                        textMessage = user.name + ' invites you to the gathering. Open http://gh3.co/#/' + config.tokenAbbreviation.gatheringInvite + '/' + token + ' and join now.';
                                                        if (textMessage.length > config.maxLengthOfSMS) {
                                                            textMessage = 'You are invited to the gathering. Open http://gh3.co/#/' + config.tokenAbbreviation.gatheringInvite + '/' + token + ' and join now.';
                                                        }
                                                    }

                                                    sms.send(user._id, phoneSMS, textMessage, function(err, utr) {
                                                        if (err) {
                                                            res.send({
                                                                status: 'ok',
                                                                msg: 'Invitation was saved into database, but something went wrong with sending text message to ' + phoneSMS + ' phone number!',
                                                                error: err.error,
                                                                data: {
                                                                    gatheringInvite: {
                                                                        _id: mgr.insertedIds[0],
                                                                        phone: phone,
                                                                        phoneSMS: phoneSMS,
                                                                        token: token
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            res.send({
                                                                status: 'ok',
                                                                msg: 'Invitation was successfully saved and sent to ' + phoneSMS + '!',
                                                                data: {
                                                                    gatheringInvite: {
                                                                        _id: mgr.insertedIds[0],
                                                                        phone: phone,
                                                                        phoneSMS: phoneSMS,
                                                                        token: token
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

api['gathering-invites/check-belonging.json'] = function (req, res) {
    var token = req.payload.token;

    if (!token) {
        res.send({
            status: 'error',
            msg: 'Token is missing!'
        });
        return;
    }

    if (token.length !== config.tokenLength) {
        res.send({
            status: 'error',
            msg: 'Token is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatheringInvites').findOne({
        token: token
    }, {
        gatheringId: true,
        phoneSMS: true,
        phone: true,
        areacodeId: true,
        respond: true,
        token: true
    }, function(err, gatheringInvite) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gatheringInvite) {
            res.send({
                status: 'error',
                msg: 'Gathering invite does not exist.'
            });
        } else {
            if (!gatheringInvite.phoneSMS) {
                res.send({
                    status: 'error',
                    msg: 'Gathering invite does not contain full phone number.'
                });
                return;
            }

            mongo.db.collection('gatherings').findOne({
                _id: ObjectId(gatheringInvite.gatheringId),
                isRemoved: {
                    $ne: true
                }
            }, {
                _id: true,
                title: true
            }, function(err, gathering) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!gathering) {
                    res.send({
                        status: 'error',
                        msg: 'Gathering does not exist.'
                    });
                } else {

                    mongo.db.collection('users').findOne({
                        phoneSMS: gatheringInvite.phoneSMS
                    }, {
                        _id: true
                    }, function(err, user) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else if (!user) {
                            res.send({
                                status: 'ok',
                                msg: 'Invite does not belong to any user.',
                                data: {
                                    gatheringInvite: gatheringInvite,
                                    gathering: gathering,
                                    user: false
                                }
                            });
                        } else {
                            res.send({
                                status: 'ok',
                                msg: 'Gathering invite does belong to the user.',
                                data: {
                                    gatheringInvite: gatheringInvite,
                                    gathering: gathering,
                                    user: user
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

api['gathering-invites/login.json'] = function (req, res) {
    var phone = req.payload.phone;
    var token = req.payload.token;
    var gatheringId = req.payload.gatheringId;
    var areacode = req.payload.areacode;

    if (!areacode) {
        res.send({
            status: 'error',
            msg: 'You have to select your area!'
        });
        return;
    }

    // convert double zero into plus
    if (areacode.substring(0,2) === '00') {
        areacode = '+' + areacode.slice(2);
    }

    // starting with plus sign
    if (areacode.charAt(0) !== '+') {
        res.send({
            status: 'error',
            msg: 'Area code should start with plus sign!'
        });
        return;
    }

    if (areacode.length < 2) {
        res.send({
            status: 'error',
            msg: 'Area code is too short!'
        });
        return;
    }

    if (!phone) {
        res.send({
            status: 'error',
            msg: 'Phone is missing!'
        });
        return;
    }

    // convert international phone number to local
    phone = phone.replace(areacode, 0);

    // starting with digit zero
    if (phone.charAt(0) !== '0') {
        res.send({
            status: 'error',
            msg: 'Phone should start with digit zero!'
        });
        return;
    }

    // only digits
    var isNum = true;
    isNum = /^[\d+]+$/.test(phone);
    if (!isNum) {
        res.send({
            status: 'error',
            msg: 'Phone should contain only digits.'
        });
        return;
    }

    if (phone.length < config.minPhoneLength) {
        res.send({
            status: 'error',
            msg: 'Phone is too short!'
        });
        return;
    }

    if (!token) {
        res.send({
            status: 'error',
            msg: 'Token is missing!'
        });
        return;
    }

    if (token.length !== config.tokenLength) {
        res.send({
            status: 'error',
            msg: 'Token is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatheringInvites').findOne({
        token: token
    }, function(err, gatheringInvite) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gatheringInvite) {
            res.send({
                status: 'error',
                msg: 'Login via invite did not succeed. Gathering invite does not exist.'
            });
        } else {
            if (!gatheringInvite.phoneSMS) {
                res.send({
                    status: 'error',
                    msg: 'Login via invite did not succeed. Gathering invite does not contain full phone number.'
                });
                return;
            }

            if (gatheringInvite.repond === 'rejected') {
                res.send({
                    status: 'error',
                    msg: 'Login via invite did not succeed. Gathering invite was rejected.'
                });
                return;
            }

            mongo.db.collection('areacodes').findOne({
                areacode: areacode
            }, function(err, area) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!area) {
                    res.send({
                        status: 'error',
                        msg: 'Login via invite did not succeed. We not support area ' + areacode + '!'
                    });
                } else {
                    var areacodeId = area._id;

                    // users collections
                    var users = mongo.db.collection('users');

                    // finding the user
                    users.findOne({
                        phone: phone,
                        areacodeId: ObjectId(areacodeId)
                    }, function(err, user) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else if (user) {
                            if (user.phoneSMS !== gatheringInvite.phoneSMS) {
                                res.send({
                                    status: 'error',
                                    msg: 'Login via invite did not succeed. User\'s phone does not match with the phone in gathering invite!'
                                });
                            } else {
                                session.set(user._id, user.phone, user.password, req.headers.host, req.headers['user-agent'], function(err, utr) {
                                    if (err) {
                                        res.send({
                                            status: 'error',
                                            msg: 'We were not able to establish session for you.',
                                            error: err.error
                                        });
                                    } else {
                                        res.send({
                                            status: 'ok',
                                            msg: 'Login succeeded!',
                                            data: {
                                                session: utr.session,
                                                userId: user._id,
                                                gatheringId: gatheringInvite.gatheringId,
                                                token: token
                                            }
                                        });
                                    }
                                });
                            }
                        } else {
                            res.send({
                                status: 'error',
                                msg: 'Login via invite did not succeed. User with ' + phone + ' phone number in area code ' + areacode + ' does not exist!'
                            });
                        }
                    });
                }
            });
        }
    });
};

api['gathering-invites/register.json'] = function (req, res) {
    var phone = req.payload.phone;
    var token = req.payload.token;
    var areacode = req.payload.areacode;
    var doublecheck = req.payload.doublecheck;

    if (!areacode) {
        res.send({
            status: 'error',
            msg: 'You have to select your area!'
        });
        return;
    }

    // convert double zero into plus
    if (areacode.substring(0,2) === '00') {
        areacode = '+' + areacode.slice(2);
    }

    // starting with plus sign
    if (areacode.charAt(0) !== '+') {
        res.send({
            status: 'error',
            msg: 'Area code should start with plus sign!'
        });
        return;
    }

    if (areacode.length < 2) {
        res.send({
            status: 'error',
            msg: 'Area code is too short!'
        });
        return;
    }

    if (!phone) {
        res.send({
            status: 'error',
            msg: 'Phone is missing!'
        });
        return;
    }

    // convert international phone number to local
    phone = phone.replace(areacode, 0);

    // starting with digit zero
    if (phone.charAt(0) !== '0') {
        res.send({
            status: 'error',
            msg: 'Phone should start with digit zero!'
        });
        return;
    }

    // only digits
    var isNum = true;
    isNum = /^[\d+]+$/.test(phone);
    if (!isNum) {
        res.send({
            status: 'error',
            msg: 'Phone should contain only digits.'
        });
        return;
    }

    if (phone.length < config.minPhoneLength) {
        res.send({
            status: 'error',
            msg: 'Phone is too short!'
        });
        return;
    }

    if (!token) {
        res.send({
            status: 'error',
            msg: 'Token is missing!'
        });
        return;
    }

    if (token.length !== config.tokenLength) {
        res.send({
            status: 'error',
            msg: 'Token is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatheringInvites').findOne({
        token: token
    }, function(err, gatheringInvite) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gatheringInvite) {
            res.send({
                status: 'error',
                msg: 'Registration via invite did not succeed. Gathering invite does not exist.'
            });
        } else {
            if (!gatheringInvite.phoneSMS) {
                res.send({
                    status: 'error',
                    msg: 'Registration via invite did not succeed. Gathering invite does not contain full phone number.'
                });
                return;
            }

            if (gatheringInvite.repond === 'rejected') {
                res.send({
                    status: 'error',
                    msg: 'Registration via invite did not succeed. Gathering invite was rejected.'
                });
                return;
            }

            mongo.db.collection('areacodes').findOne({
                areacode: areacode
            }, function(err, area) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!area) {
                    res.send({
                        status: 'error',
                        msg: 'Registration via invite did not succeed. We sadly do not support area ' + areacode + ' yet!'
                    });
                } else {
                    // phone for SMS
                    var phoneSMS = area.areacode + phone.substr(1);

                    if (phoneSMS !== gatheringInvite.phoneSMS) {
                        res.send({
                            status: 'error',
                            msg: 'Registration via invite did not succeed. Phone ' + phone + ' does not match with the phone in gathering invite!'
                        });
                    }

                    // users collections
                    var users = mongo.db.collection('users');

                    // finding the user
                    users.findOne({
                        phone: phone
                    }, function(err, user) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else if (user) {
                            res.send({
                                status: 'error',
                                msg: 'Registration via invite did not succeed. User with phone ' + phone + ' already exists!'
                            });
                        } else if (!doublecheck) {
                            res.send({
                                status: 'error',
                                msg: 'Registration via invite did not succeed. Please double check the phone number!',
                                error: 'doublecheck',
                                data: {
                                    phoneSMS: phoneSMS
                                }
                            });
                        } else {
                            // generating random password from readable letters
                            var generatedPassword = '';
                            for (var i = 0; i < config.minPasswordLength; i++) {
                                generatedPassword += config.readableLetters.charAt(Math.floor(Math.random() * config.readableLetters.length));
                            }
                            generatedPassword = generatedPassword.charAt(0).toUpperCase() + generatedPassword.slice(1);

                            var hash = require('crypto').createHash('sha256').update(generatedPassword + secure.salt).digest('hex');

                            users.insert({
                                phone: phone,
                                phoneSMS: phoneSMS,
                                password: hash,
                                created: new Date().getTime(),
                                areacodeId: ObjectId(area._id),
                                invitedNoobie: true,
                                invitedBy: ObjectId(gatheringInvite.createdBy)
                            }, function(err, mgr) {
                                if (err) {
                                    res.send({
                                        status: 'error',
                                        msg: 'Database error!',
                                        error: err.message
                                    });
                                } else if (!mgr.result.ok || !mgr.result.n) {
                                    res.send({
                                        status: 'error',
                                        msg: 'Database inserting error!'
                                    });
                                } else {
                                    user = mgr.ops[0];

                                    session.set(user._id, user.phone, user.password, req.headers.host, req.headers['user-agent'], function(err, utr) {
                                        if (err) {
                                            res.send({
                                                status: 'error',
                                                msg: 'Hey! We have created noobie user account for you, but were not able to establish session for you.',
                                                error: err.error
                                            });
                                        } else {
                                            sessionHash = utr.session;

                                            sms.send(user._id, phoneSMS, 'Welcome to http://gh3.co/ community!', function(err, utr) {
                                                if (err) {
                                                    res.send({
                                                        status: 'ok',
                                                        msg: 'Hey! We have created noobie user account for you. We can not send you welcome text message currently, but you can totally contiue browsing!',
                                                        data: {
                                                            session: sessionHash,
                                                            userId: user._id,
                                                            gatheringId: gatheringInvite.gatheringId,
                                                            token: token
                                                        }
                                                    });
                                                } else {
                                                    res.send({
                                                        status: 'ok',
                                                        msg: 'Hey! We have created noobie user account for you. You will receive a welcome text message on your phone. Meanwhile you can continue browsing!',
                                                        data: {
                                                            session: sessionHash,
                                                            userId: user._id,
                                                            gatheringId: gatheringInvite.gatheringId,
                                                            token: token
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

api['gathering-invites/respond-on-token.json'] = function (req, res) {
    var token = req.payload.token;
    var respond = req.payload.respond;
    var user = req.user;

    if (!token) {
        res.send({
            status: 'error',
            msg: 'Token is missing!'
        });
        return;
    }

    if (token.length !== config.tokenLength) {
        res.send({
            status: 'error',
            msg: 'Token is corrupted!'
        });
        return;
    }

    if (!respond) {
        res.send({
            status: 'error',
            msg: 'Respond is missing!'
        });
        return;
    }

    // accepted/rejected
    if (!['accepted', 'rejected'].includes(respond)) {
        res.send({
            status: 'error',
            msg: 'Only \'accepted\' or \'rejected\' are acceptable responds!'
        });
        return;
    }

    mongo.db.collection('gatheringInvites').findOne({
        token: token
    }, function(err, gatheringInvite) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gatheringInvite) {
            res.send({
                status: 'error',
                msg: 'Gathering invite does not exist.'
            });
        } else {
            if (gatheringInvite.phoneSMS !== user.phoneSMS) {
                res.send({
                    status: 'error',
                    msg: 'This invitation token does not belong to you.'
                });
                return;
            }

            if (gatheringInvite.respond) {
                res.send({
                    status: 'error',
                    msg: 'You have already responded to this token.'
                });
                return;
            }

            mongo.db.collection('gatheringInvites').update({
                token: token
            }, {
                $set: {
                    respond: respond
                }
            }, function(err, mgr) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!mgr.result.ok || !mgr.result.n) {
                    res.send({
                        status: 'error',
                        msg: 'Database updating error!'
                    });
                } else {
                    if (respond === 'rejected') {
                        res.send({
                            status: 'ok',
                            msg: 'Invitation was successfully rejected!',
                        });
                    } else if (respond === 'accepted') {
                        mongo.db.collection('gatheringParticipants').insert({
                            userId: ObjectId(user._id),
                            gatheringId: ObjectId(gatheringInvite.gatheringId),
                            creatorId: ObjectId(user._id),
                            created: new Date().getTime(),
                            inviteId: ObjectId(gatheringInvite._id)
                        }, function(err, mgr) {
                            if (err) {
                                res.send({
                                    status: 'error',
                                    msg: 'Database error!',
                                    error: err.message
                                });
                            } else if (!mgr.result.ok || !mgr.result.n) {
                                res.send({
                                    status: 'error',
                                    msg: 'Database inserting error!'
                                });
                            } else {
                                res.send({
                                    status: 'ok',
                                    msg: 'Invitation was accepted and you were successfully added to the gathering!',
                                    data: {
                                        gatheringParticipant: mgr.ops[0]
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
    });
}

api['gathering-invites/find-by-gathering.json'] = function (req, res) {
    var gatheringId = req.payload.gatheringId;
    var user = req.user;

    if (!gatheringId) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is missing!'
        });
        return;
    }

    if (gatheringId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatherings').findOne({
        _id: ObjectId(gatheringId),
        isRemoved: {
            $ne: true
        }
    }, function(err, gathering) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gathering) {
            res.send({
                status: 'error',
                msg: 'Gathering does not exist.'
            });
        } else {
            if (String(gathering.userId) !== String(req.user._id)) {
                res.send({
                    status: 'error',
                    msg: 'You have to be the leader of gathering if you want to see invites!'
                });
                return;
            }

            mongo.db.collection('gatheringInvites').find({
                gatheringId: ObjectId(gatheringId)
            }, {
                phone: true,
                phoneSMS: true,
                respond: true,
                token: true
            }).toArray(function(err, gatheringInvites) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else {
                    res.send({
                        status: 'ok',
                        msg: 'Here are intvites of the gathering!',
                        data: {
                            gatheringInvites: gatheringInvites
                        }
                    });
                }
            });
        }
    });
};

api['gathering-invites/remove-one.json'] = function (req, res) {
    var gatheringId = req.payload.gatheringId;
    var gatheringInviteId = req.payload.gatheringInviteId;
    var user = req.user;

    if (!gatheringId) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is missing!'
        });
        return;
    }

    if (gatheringId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering ID is corrupted!'
        });
        return;
    }

    if (!gatheringInviteId) {
        res.send({
            status: 'error',
            msg: 'Gathering invite ID is missing!'
        });
        return;
    }

    if (gatheringInviteId.length !== 24) {
        res.send({
            status: 'error',
            msg: 'Gathering invite ID is corrupted!'
        });
        return;
    }

    mongo.db.collection('gatherings').findOne({
        _id: ObjectId(gatheringId),
        isRemoved: {
            $ne: true
        }
    }, function(err, gathering) {
        if (err) {
            res.send({
                status: 'error',
                msg: 'Database error!',
                error: err.message
            });
        } else if (!gathering) {
            res.send({
                status: 'error',
                msg: 'Gathering does not exist.'
            });
        } else {
            if (String(gathering.userId) !== String(user._id)) {
                res.send({
                    status: 'error',
                    msg: 'You can not remove an invite if you are not the leader of gathering!'
                });
                return;
            }

            mongo.db.collection('gatheringInvites').findOne({
                gatheringId: ObjectId(gatheringId),
                _id: ObjectId(gatheringInviteId)
            }, function(err, gatheringInvite) {
                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Database error!',
                        error: err.message
                    });
                } else if (!gatheringInvite) {
                    res.send({
                        status: 'error',
                        msg: 'Gathering invite does not exist!'
                    });
                } else {
                    mongo.db.collection('gatheringInvites').remove({
                        gatheringId: ObjectId(gatheringId),
                        _id: ObjectId(gatheringInviteId)
                    }, function(err, mgr) {
                        if (err) {
                            res.send({
                                status: 'error',
                                msg: 'Database error!',
                                error: err.message
                            });
                        } else if (!mgr.result.ok || !mgr.result.n) {
                            res.send({
                                status: 'error',
                                msg: 'Database removing error!'
                            });
                        } else {
                            res.send({
                                status: 'ok',
                                msg: 'Gathering invite was successfully removed from gathering!'
                            });
                        }
                    });
                }
            });
        }
    });
};

module.exports = api;
