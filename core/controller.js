var zlib = require('zlib');
var config = require('./config.js');
var secure = require('./secure.js');
var router = require('./router.js');
var mime = require('./mime.js');
var mongo = require('./mongo.js');
var utils = require('../utils/utils.js');
var ObjectId = require('mongodb').ObjectID;

module.exports = function(req, res) {

    // preparing response send function
    res.send = function (data, headers) {

        // status code
        var statusCode = headers && headers.hasOwnProperty('Status-Code') ? headers['Status-Code'] : 200;

        // string should be simply outputted
        if (typeof data === 'string') {
            res.writeHead(statusCode);
            res.end(data);
            return
        }

        // head
        var head = {
            'Cache-Control': 'private, no-cache, no-store, must-revalidate',
        }

        // allow cors
        if (req.headers['origin']) {
            head['Access-Control-Allow-Origin'] = req.headers['origin']
            head['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Session'
            head['Access-Control-Allow-Credentials'] = true
        }

        // accept encoding
        var acceptEncoding = req.headers['accept-encoding'] ? req.headers['accept-encoding'] : '';

        // response data
        var data = (typeof data != 'object') ? {} : data;

        // determing if returning a static file or a json
        if (data.hasOwnProperty('file') && data.hasOwnProperty('name')) {

            // contant type by mime type
            if (data.name.split('.').length > 1) {
                var extension = req.path.split('.').pop();
                head['Content-Type'] = mime[extension];
            } else {
                head['Content-Type'] = 'text/plain';
            }

            // response data
            var responseData = data.file;

            // encode data encoding
            if (secure.httpEncodingEnabled && head['Content-Type']) {
                if (head['Content-Type'].match(/\bimage\b/)) {
                    // do not perform any encoding for images at any time
                } else if (acceptEncoding.match(/\bdeflate\b/)) {
                    head['Content-Encoding'] = 'deflate';
                    responseData = zlib.deflateSync(responseData);
                } else if (acceptEncoding.match(/\bgzip\b/)) {
                    head['Content-Encoding'] = 'gzip';
                    responseData = zlib.gzipSync(responseData);
                }
            }

            // response write
            res.writeHead(statusCode, head);
            res.write(responseData, 'binary');

        } else {

            // content type is json
            head['Content-Type'] = 'text/json';

            // stringifying respons json data
            try {
                data = JSON.stringify(data, function(key, value) {
                    if (key == 'rawSocket') {
                        return '...';
                    } else {
                        return value;
                    };
                });
            } catch (err) {
                data = '{"status":"error","message":"Yikes! Response can not be stringified!"}';
            }

            // encode data
            var responseData = data;

            // encode data encoding
            if (secure.httpEncodingEnabled) {
                if (acceptEncoding.match(/\bdeflate\b/)) {
                    head['Content-Encoding'] = 'deflate';
                    responseData = zlib.deflateSync(responseData);
                } else if (acceptEncoding.match(/\bgzip\b/)) {
                    head['Content-Encoding'] = 'gzip';
                    responseData = zlib.gzipSync(responseData);
                }
            }

            // response write
            res.writeHead(statusCode, head);
            res.write(responseData, 'utf8');
        }

        res.end();
    }

    // checking host 
    var host = req.headers.host;

    // redirect to url without www
    if (host.substring(0,4) === 'www.') {
        var newHost = 'http://' + host.substring(4);
        res.writeHead(302, {
            'Location': newHost
        });
        res.end();
        return;
    }

    // stop it if host is not among the valid ones
    if (config.hosts.indexOf(host) === -1) {
        res.send('404 Found', {
            statusCode: 404
        });
        return;
    }

    // parsing cookie
    req.cookie = {};
    if (req.headers.hasOwnProperty('cookie')) {
        var cookie;
        var cookies = req.headers.cookie.split('; ');
        try {
            for (var c in cookies) {
                cookie = cookies[c].split('=');
                req.cookie[cookie[0]] = cookie[1];
            }
        } catch (err) {
            res.send({
                status: 'out',
                msg: 'Cookie is damaged.'
            });
            return;
        }
    }

    // request path is requesting url wihout params
    req.path = req.url.split('?', 1)[0]
    req.segments = req.path.substring(1).split('/');

    // checking uri segments
    if (req.segments[0] === config.crucialUriSegment.photo) {
        // session needed if requesting photo
    } else if (req.segments[0] === config.crucialUriSegment.api && config.openApi.indexOf(req.path) === -1) {
        // session needed if requesting non open api
    } else {
        router(req, res);
        return;
    }

    // getting session from session header
    var session = false;
    if (req.headers.hasOwnProperty('session')) {
        session = req.headers.session
    }

    // if session is missing, skip it directly to router
    if (!session) {
        router(req, res);
        return;
    }

    // if url is root, skip it directly to router
    if (req.url === '/') {
        router(req, res);
        return;
    }

    // if url is pointing to frontend folder, skip it directly to router
    if (req.segments[0] !== config.crucialUriSegment.api && req.segments[0] !== config.crucialUriSegment.photo) {
        router(req, res);
        return;     
    }

    // if url is a token, skip it directly to router
    var tokenAbbreviations = Object.values(config.tokenAbbreviation);
    if (tokenAbbreviations.includes(req.segments[0])) {
        router(req, res);
        return;
    }

    // decrypt session
    var decipher = require('crypto').createDecipher('aes256', secure.key);

    try {
        var sessionString = decipher.update(session, 'hex', 'utf8') + decipher.final('utf8');
        var sessionJSON = JSON.parse(sessionString);

    } catch (err) {
        res.send({
            status: 'out',
            msg: 'Session looks weird. Please log in again!'
        });
        return;
    }

    if (!sessionJSON || typeof sessionJSON != 'object') {
        res.send({
            status: 'out',
            msg: 'Session is somehow broken. Please log in again!'
        });
        return;
    }

    // check session data
    if (!sessionJSON.userId || !sessionJSON.phone || !sessionJSON.password || !sessionJSON.ip || !sessionJSON.agent || !sessionJSON.key || !sessionJSON.created) {
        res.send({
            status: 'out',
            msg: 'Session does not contain all the needed data. Please log in again.'
        });
        return;
    }

    // check user host adequacy
    if (sessionJSON.ip != req.headers.host) {
        res.send({
            status: 'out',
            msg: 'Your host is different than the one in session. Please log in again.'
        });
        return;   
    }

    // check user agent adequacy
    if (sessionJSON.agent != req.headers['user-agent']) {
        res.send({
            status: 'out',
            msg: 'Your agent is different than the one in session. Please log in again.'
        });
        return;   
    }

    // check app key adequacy
    if (sessionJSON.key != secure.key) {
        res.send({
            status: 'out',
            msg: 'Current secure key is different than the one in session. Please log in again.'
        });
        return;   
    }

    // users collections
    var users = mongo.db.collection('users');

    // finding the user
    users.findOne({
        _id: ObjectId(sessionJSON.userId)
    }, function(err, user) {
        if (err) {
            res.send({
                status: 'error',
                message: 'Database error!',
                error: err.message
            });
        } else if (!user) {
            res.send({
                status: 'out',
                msg: 'You do not exist any more in our system.'
            });
        } else {
            // check user password adequacy
            var userPassword = utils.hidePass(user.password);
            if (sessionJSON.password != userPassword) {
                res.send({
                    status: 'out',
                    msg: 'Hashed password in the database does not match with the one in session. Please log in again.'
                });
                return;   
            }

            req.user = user;
            router(req, res);
        }
    });
};
