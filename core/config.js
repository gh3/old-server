module.exports = {
    port: 80,
    dirnames: {
        api: '../api',
        core: '../core',
        mongoData: '../mongo_data',
        nodeModules: '../node_modules',
        uploads: '../uploads',
        temporaryUploads: '../uploads/tmp',
        gatheringPhotos: '../uploads/gathering-photos',
        frontendBuild: '../../frontend/build'
    },
    folder: {}, // absolute path of dirnames
    crucialUriSegment: { // controller and router related
        api: 'api',
        photo: 'photo',
    },
    api: {},
    openApi: [
        '/api/other/public-files.json',
        '/api/other/api-calls.json',
        '/api/auth/register.json',
        '/api/auth/login.json',
        '/api/auth/password-reset.json',
        '/api/auth/password-recovery.json',
        '/api/auth/check-recovery-token.json',
        '/api/auth/password-set.json',
        '/api/areacodes/find.json',
        '/api/gathering-invites/check-belonging.json',
        '/api/gathering-invites/login.json',
        '/api/gathering-invites/register.json',
        '/api/user/confirm-phone-update.json',
        '/api/cron/mark-gatherings-as-removed.json',
    ],
    hosts: [
        '46.101.203.12',
        'localhost',
        '127.0.0.1',
        'gh3.co',
        'dev.gh3.co',
    ],
    readableLetters: 'abcdefghjkmnprstuvz123456789',
    services: {
        clickatell: {
            apiId: '3588687',
            authToken: 'n1Sjci_4vA4Je5xpZqpyzy7hSjqigM53FErCjrCb28LTDmR2sdGgf1MLuuIWWx',
        }
    },
    maxLengthSMS: 100, // characters
    passwordResetWaitTime: 15, // min
    passwordRecoveryWaitTime: 15, // min
    passwordRecoveryValidTime: 60, // min
    tokenLength: 16, // characters
    tokenAbbreviation: {
        gatheringInvite: 'gi',
        passwordRecovery: 'pr',
        phoneUpdate: 'pu',
    },
    minPasswordLength: 8, // characters
    minPhoneLength: 6, // characters
    sendingSMSsEnabled: true,
    magicNumbers: {
        jpg: 'ffd8',
        png: '89504e47',
        gif: '47494638',
    },
    specialCharsReplacements: {
        'ž': 'z',
        'Ž': 'Z',
        'š': 's',
        'Š': 'S',
        'č': 'c',
        'Č': 'C',
        'ć': 'c',
        'Ć': 'C',
        'đ': 'dz',
        'Đ': 'Dz',
    },
    gatheringRemovingPeriod: 1, // days
}