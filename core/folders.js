var fs = require('fs');
var path = require('path');
var config = require('./config.js');
var mongo = require('./mongo.js');

module.exports.check = function() {

    var absolutePath;

    for (var name in config.dirnames) {
        absolutePath = path.normalize(__dirname + '/' + config.dirnames[name] + '/');
        config.folder[name] = absolutePath;

        if (!fs.existsSync(absolutePath)) {
            console.log('FOLDER ERROR - DIRECTORY DOES NOT EXIST - PLEASE CREATE ' + absolutePath);
            return;
        }
    }
    
    mongo.open();
}
