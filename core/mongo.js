var mongo = require('mongodb').MongoClient;
var api = require('./api.js');
var secure = require('./secure.js');

module.exports.open = function() {
    // connect to mongo
    mongo.connect('mongodb://' + secure.mongo.user + ':' + secure.mongo.pass + '@' + secure.mongo.host + ':' + secure.mongo.port + '/' + secure.mongo.name, function(err, db) {
        if (err) {
            console.error('SERVER ERROR - CONNECTION TO MONGO FAILED - CHECK IF ' + secure.mongo.name + ' IS RUNNING');
        } else {
            module.exports.db = db;
            api.collect();
        }
    });
};
