var http = require('http');
var config = require('./config.js');
var controller = require('./controller.js');

module.exports.start = function() {

    // create http server, passing request
    var server = http.createServer(controller);

    // on error
    server.once('error', function(err) {
        if (err.code === 'EADDRINUSE') {
            // port is currently in use
            console.error('SERVER ERROR - PORT ' + config.port + ' IS IN USE - KILL THE PROCESS WHICH IS USING IT AND RESTART THE SERVER!');
        } else if (err.code === 'EACCES') {
            console.error('SERVER ERROR - YOU DO NOT HAVE PROPER PERMISSIONS TO RUN HTTP - TRY WITH ROOT/ADMIN ACCESS');
        } else {
            console.error('SERVER ERROR - AN ERROR WHILE STARTING HTTP');
            console.error(err);
        }
        server.close();
    });

    // on listening
    server.once('listening', function() {
        console.log('SERVER OK');
    });

    // start listening on port
    server.listen(config.port);
}
