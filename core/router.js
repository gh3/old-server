var fs = require('fs');
var config = require('./config.js');
var controller = require('./controller.js');
var formidable = require('formidable');
var util = require('util');
var path = require('path');

module.exports = function (req, res) {

    if (req.segments[0] === config.crucialUriSegment.api) {

        // checking api type
        if (req.path.slice(-5) !== '.json') {
            res.send({
                status: 'error',
                msg: 'Requesting \'.json\' API or not?'
            });
            return;
        }

        // checking segments
        if (req.segments.length !== 3) {
            res.send({
                status: 'error',
                msg: 'It seems as API url segments are not valid.'
            });
            return;
        }

        // kill it if api is not public and user is not authorized
        if (config.openApi.indexOf(req.path) === -1 && !req.user) {
            res.send({
                status: 'out',
                msg: 'API available only to authorized users. Please log in!',
                hint: '/api/auth/login.json'
            });
            return;
        }

        // request api
        req.api = {};
        req.api.name = req.segments[1];
        req.api.address = req.segments[1] + '/' + req.segments[2];
        req.api.file = '../api/' + req.api.name + '.js';

        // check if api exist to handle this request
        if (config.api.hasOwnProperty(req.api.name) && config.api[req.api.name].indexOf(req.path) !== -1) {

            // init formidable
            var form = new formidable.IncomingForm();
            form.uploadDir = config.folder.temporaryUploads;
            form.keepExtensions = true;

            // parsing data
            form.parse(req, function(err, fields, files) {

                if (err) {
                    res.send({
                        status: 'error',
                        msg: 'Yikes! Internal form parsing error!'
                    });

                } else {

                    if (Object.keys(files).length) {

                        if (files.hasOwnProperty('null')) {
                            res.send({
                                status: 'error',
                                msg: 'Each uploaded file should have a unique key!'
                            });
                            return;
                        }
                    }

                    req.payload = fields;
                    req.upload = files;

                    // calling actual api
                    var api = require(req.api.file);
                    api[req.api.address](req, res);
                }

            });

        } else {
            res.send({
                status: 'error',
                msg: 'Yikes! API not found!'
            });
        }

    } else if (req.segments[0] === config.crucialUriSegment.photo) {

        // proceed only if user session is present
        if (!req.user) {
            res.send({
                status: 'out',
                msg: 'Image available only to authorized users. Please log in!',
                hint: '/api/auth/login.json'
            });
            return;
        }

        // calling image api
        var api = require('../api/gathering-photos.js');
        api['gathering-photos/get-one.json'](req, res);

    } else {

        // if hitting root pathname
        if (req.path === '/') {
            req.path = '/index.html'
        }

        // frontend build files and folders location
        var location = path.normalize(config.folder.frontendBuild + req.path);

        // check the existence of requested location
        fs.stat(location, function(err, stats) {

            if (err) {
                res.send({
                    status: 'error',
                    msg: 'File not found!'
                }, {
                    'Status-Code': 404
                });

            } else if (stats.isFile()) {

                // respond with wanted file
                fs.readFile(location, 'binary', function(err, file) {

                    if (err) {
                        res.send({
                            status: 'error',
                            msg: 'Reading file binary crashed!'
                        }, {
                            'Status-Code': 500
                        });
                    } else {
                        res.send({
                            file: file,
                            name: req.path
                        });
                    }
                });

            } else {
                res.send({
                    status: 'error',
                    msg: 'Requested location is not a file!'
                }, {
                    'Status-Code': 404
                });
            }
        });

    }
}
