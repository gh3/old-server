var fs = require('fs');
var config = require('./config.js');
var server = require('./server.js');

module.exports.collect = function() {

    // looks for API in the folder
    fs.readdirSync(config.folder.api).forEach(function(file) {

        // ignore files which starts with dot
        if (file.charAt(0) === '.') {
            return;
        }

        // get api name and file type from the file
        var fileName = file.split('.');

        var apiName = fileName[0];
        var filetype = fileName[1];

        // check if .js file
        if (filetype !== 'js') {
            console.log('API ERROR - FILE NAME IS WRONG');
            return;
        }

        // require each api file
        var api = require(config.folder.api + file);

        for (var pathname in api) {

            // check if host is okay
            if (pathname.indexOf(apiName + '/') !== 0) {
                console.log('API ERROR - API PATHNAME IS WRONG');
                return;
            }

            // remember the api
            if (!config.api.hasOwnProperty(apiName)) {
                config.api[apiName] = [];
            }

            config.api[apiName].push('/api/' + pathname);
        }
        
    });

    // server
    server.start();
}
