var config = require('../core/config.js');
var secure = require('../core/secure.js');
var mongo = require('../core/mongo.js');
var utils = require('./utils.js');
var ObjectId = require('mongodb').ObjectID;

module.exports.set = function(userId, phone, password, host, agent, cb) {

    if (!userId) {
        cb({
            error: 'User ID is missing!'
        });
        return;
    }

    if (typeof userId !== 'object') {
        cb({
            error: 'User ID must be object!'
        });
        return;
    }

    if (!phone) {
        cb({
            error: 'Phone is missing!'
        });
        return;
    }

    if (!password) {
        cb({
            error: 'Password is missing!'
        });
        return;
    } else {
        password = utils.hidePass(password);
    }

    if (!host) {
        cb({
            error: 'Host is missing!'
        });
        return;
    }

    if (!agent) {
        cb({
            error: 'Agent is missing!'
        });
        return;
    }

    if (!cb) {
        cb({
            error: 'Callback is missing!'
        });
        return;
    }

    // cipher
    var cipher = require('crypto').createCipher('aes256', secure.key);

    // session
    var sessionJSON = {
        userId: userId,
        phone: phone,
        password: password,
        ip: host,
        agent: agent,
        key: secure.key,
        created: new Date().getTime()
    };

    var sessionString = JSON.stringify(sessionJSON);
    var session = cipher.update(sessionString, 'utf8', 'hex') + cipher.final('hex');

    // return session in callback
    cb(false, {
        success: 'Crypted session was created!',
        session: session
    });
}