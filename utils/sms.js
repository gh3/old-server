var http = require('http');
var config = require('../core/config.js');
var mongo = require('../core/mongo.js');

module.exports.send = function(userId, phone, text, cb) {

    // check if sending SMS is enabled or disabled
    if (!config.sendingSMSsEnabled) {
        cb({
            error: 'Text messages are currently disabled!'
        });
        return;
    }
    // is userId
    if (!userId) {
        cb({
            error: 'User ID is missing!'
        });
        return;
    }

    if (typeof userId !== 'object') {
        cb({
            error: 'User ID must be object!'
        });
        return;
    }

    // is phone
    if (!phone) {
        cb({
            error: 'Phone is missing!'
        });
        return;
    }

    // refactor receivers into array
    var receivers = [];
    if (phone instanceof Array) {
        receivers = phone;
    } else {
        receivers.push(phone);
    }

    // only digits
    var isNum = true;    
    for (var r in receivers) {
        isNum = /^[\d+]+$/.test(receivers[r]);
        if (!isNum) {
            cb({
                error: 'Phone should contain only digits. Only first letter can be plus!'
            });
            return;
        }
    }

    // is text
    if (!text) {
        cb({
            error: 'Text is missing!'
        });
        return;
    }

    // parse out invalid special characters
    var regex, invalidChar, replacementChar;
    for (invalidChar in config.specialCharsReplacements) {
        replacementChar = config.specialCharsReplacements[invalidChar];
        regex = new RegExp(invalidChar, 'g');
        text = text.replace(regex, replacementChar);
    }

    // max length
    if (text.length > config.maxLengthOfSMS) {
        cb({
            error: 'Text is too long, ' + config.maxLengthOfSMS + ' max!'
        });
        return;
    }

    // preparing data
    try {
        var postData = JSON.stringify({
            to: receivers,
            text: text,
            from: 'gh3'
        });
    } catch (err) {
        cb({
            error: 'Post data can not be stringified!'
        });
        return;
    }

    // logSMS collections
    var logSMS = mongo.db.collection('logSMS');

    // inserting logSMS
    logSMS.insert({
        userId: userId,
        to: receivers,
        text: text,
        created: new Date().getTime()
    }, function(err, mgr) {
        if (err) {
            cb({
                error: 'Database error!',
            });
        } else {

            // clickatell options
            var options = {
                hostname: 'api.clickatell.com',
                path: '/rest/message',
                method: 'POST',
                headers: {
                    'X-Version': 1,
                    'Authorization': 'Bearer ' + config.services.clickatell.authToken,
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                }
            };

            // making http request on external service
            var httpRequest = http.request(options, function(res) {

                res.setEncoding('utf8');
                res.on('data', function (chunk) {
                    data = JSON.parse(chunk);

                    if (typeof data === 'object') {
                        if (data.hasOwnProperty('error')) {
                            cb({
                                error: 'External service responded with an error. ' + data.error.description
                            });
                        } else {
                            cb(false, {
                                success: 'Message will be delivered in a bit!'
                            });
                        }
                    } else {
                        cb({
                            error: 'Error while retrieving data from external service!'
                        });
                    }
                });
            });

            httpRequest.on('error', function(err) {
                if (err.code === 'ENOTFOUND') {
                    console.log('SERVER ERROR - HOSTNAME ' + options.hostname + ' WAS NOT FOUND!');
                } else {
                    console.log('SERVER ERROR - AN ERROR WHILE SENDING SMS');
                    console.log(err);
                }

                cb({
                    error: 'Error while trying to connect to external service!'
                });
            });

            httpRequest.write(postData);
            httpRequest.end();
        }  
    });
}