This project is obsolete and not maintained anymore!

# PREPERATION #

First you need to set up your local machine.


1.
Make sure you have made a checkout of latest client code into the **client** folder which must be in the same folder as **server**.

If you hadn't, then you need to create at least these empty folders:
- **../client**
- **../client/public**

It is necessary for running the server.

2.
Create these folders inside your working server folder:
- **./uploads**
- **./uploads/tmp**
- **./uploads/gathering-photos**

These folder will contain your local uploads. If you don't have it, you won't be able to start server process later.

3.
Install all necessary node modules which are listed under "dependencies" in package.json.
You can do it with this command:
```
npm install
```

Running this command as **root/administrator/sudo** will be probably requested.

4.
Check if any packages are outdated on your machine:
```
npm outdated
```

In case there is outdated package which has "Current" version different than "Wanted", you need to updated it with command:
```
npm update
```

If nothing was returned, you can go ahead.

6.
**Note:** By default server uses **gh3-development** database on a remote server, which means you need an internet connection for development.

If you think that is stupid, feel free to set up local one.

5.
Your server is ready for running!




# RUNNING SERVER #

There are is only one commnad which you need to run to start working.

Running this command as **root/administrator/sudo** will be probably requested.

**Tip:** *You can check what all these __npm run__ commands do in the background in package.json file under "scripts".*

1.
Run node server with pm2 package:
```
npm run server
```

You should see "SERVER OK" on your screen.

No? You are doomed. Start asking question.

Yes? Open your browser and point it to http://127.0.0.1/. There is the page. Good luck!


---

If you will have problems with node processes, you can kill them by running command below:
```
ps -ef | grep node | grep -v grep | awk '{print $2}' | xargs kill
```



## PROD ##

Connecting to production VPS via SSH:
```
ssh root@46.101.203.12
```

Running a server as a bare node:
```
node /root/gh3/server/core/index.js
```

Running a sever on production with pm2 package:
```
pm2 ls
pm2 start /root/gh3/server/core/index.js --watch --ignore-watch='uploads mongo_data node_modules .git .gitignore npm-debug.log package.json README.md'
pm2 show 0
```



# MONGO #

Yes we are using MongoDB.

More here: https://www.mongodb.com/

Down below are some useful commands.


Simple connect to local mongo database if you have it:
```
mongo 127.0.0.1:27017/gh3
```

Simple connect to remote mongo database if you'll need it:
```
mongo 46.101.210.100:27017/gh3
```

If bare **mongo** command won't work for you, try using:
```
./node_modules/.bin/mongo
```
---

Backuping production database:
```
mongodump --host 46.101.118.156 --port 27600 --out ./mongo_data/mongodump/ --db gh3-production -u "produser" -p "XXXXXXXX"
```

Restoring database to the development environment:
```
mongorestore --host 46.101.118.156 --port 27600 ./mongo_data/mongodump/gh3-production --drop -d gh3-development -u "devuser" -p "XXXXXXXX"
```



# PROD #

Connecting to production VPS via SSH:
```
ssh root@46.101.118.156
```

Starting mongo daemon process in production environment:
```
mongod --dbpath /root/mongo_data --port 27600 --fork --auth --logpath /var/log/mongodb/mongod.log
```

Connecting to the gh3-development database in production:
```
mongo --port 27600 -u "devuser" -p "PASSWORD" --authenticationDatabase "gh3-development"
use gh3-development
show collections
```

Connecting to the gh3-production database in production:
```
mongo --port 27600 -u "produser" -p "PASSWORD" --authenticationDatabase "gh3-production"
use gh3-production
show collections
```

Create an user account in admin remote database:
```
use gh3-development
db.createUser(
  {
    user: "NAME",
    pwd: "PASSWORD",
    roles: [ { role: "readWrite", db: "gh3-development" } ]
  }
)
```